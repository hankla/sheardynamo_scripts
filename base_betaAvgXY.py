#-------------------------------------------------------
# OVERVIEW: This script uses Visit to extract the magntic
# beta parameter (gas/magnetic pressure) as a function of
# height z, having averaged over x and y. It does this for 
# each time step, outputting the data to a txt file that 
# can then immediately be read by matlab using importdata()
# and also easily averaged over to see the time evolution of
# the average value of beta. 
# THIS SCRIPT IS A TEMPLATE THAT WILL BE COPIED/OVERWRITTEN BY GENERATE_BETAAVGXY.SH
#
# INPUTS: 
# -visitpath: path to visit modules. Shouldn't need to be changed 
# -datapath: path to database to be analyzed, which should be .vtks output by athena with primitive variables. Also needs to include ' database' at the end to function with OpenDatabase
# -filename: name of the file where the condensed data will be output (to be read by matlab)
# OUTPUTS: 
# -f: the file where the condensed data will be output. Will be named as filename. Rows are the by values at one time...there should be 64 columns, since the z-resolution is 64 zones. The number of rows is the time...usually starting at t=0 and progressing in intervals of 0.1 orbits (as specified in the athena input file). Time is not explicitly a column, neither are the z-coordinates. 
# Author: Lia  Hankla
#     Modifications
# 2018/02/13: Created from Visit script produced through command's record function (unnecessary fluff removed)
# 2018/02/14: Began converting to automated version.
#------------------------------------------------------
filename='basefilename'
visitpath="/usr/local/misc/visit/visit2_12_3.linux-x86_64/2.12.3/linux-x86_64/lib/site-packages"
datapath="localhost:/home/hankla/data_work/trial_basetrial_data/athvar_cspecs_mainvarfile_full_data/final_vtk_prim/athvar_cspecs_mainvar.*.vtk database"
import sys
sys.path.append(visitpath)
from visit import *
#Launch() #comment out when doing batch

OpenDatabase(datapath, 0)
DefineVectorExpression("Coords", "coord(mesh)")
DefineScalarExpression("zz", "Coords[2]")
DefineScalarExpression("beta", "point_constant(mesh,1)*8*3.14*pressure/sqrt(cell_centered_B_magnitude)") #point_constant necessary to make visit distinguish between nodal and centered
AddPlot("Curve", "operators/DataBinning/1D/mesh", 1, 1)
DataBinningAtts = DataBinningAttributes()
DataBinningAtts.numDimensions = DataBinningAtts.One  # One, Two, Three
DataBinningAtts.dim1BinBasedOn = DataBinningAtts.Z  # X, Y, Z, Variable
DataBinningAtts.dim1SpecifyRange = 0
DataBinningAtts.dim1NumBins = 64

DataBinningAtts.outOfBoundsBehavior = DataBinningAtts.Clamp  # Clamp, Discard
DataBinningAtts.reductionOperator = DataBinningAtts.Average  # Average, Minimum, Maximum, StandardDeviation, Variance, Sum, Count, RMS, PDF
DataBinningAtts.varForReduction = "beta"
DataBinningAtts.emptyVal = 0
DataBinningAtts.outputType = DataBinningAtts.OutputOnBins  # OutputOnBins, OutputOnInputMesh
DataBinningAtts.removeEmptyValFromCurve = 0
SetOperatorOptions(DataBinningAtts, 1)
DrawPlots()

tmax=TimeSliderGetNStates()
f = open(filename,'a')
for j in range(tmax):
    vals = GetPlotInformation()["Curve"]
    for i in range(len(vals)/2):
        idx=i*2+1
        f.write(str(vals[idx])+' ')
    TimeSliderNextState()
    f.write('\n')
f.close()

exit() #needed to run this template repeatedly. otherwise will just enter interactive mode.
