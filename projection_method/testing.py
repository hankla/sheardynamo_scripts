"""
Load in trials and run projection method.
"""
import numpy as np
import projection_method as pm
import matplotlib.pyplot as plt

emfx = np.loadtxt("/u/ahankla/analysis_sheardynamo/data_reduced/xyazte_emf/xyazte_emfx_hgbLAIF_r64z4b4000znf_q15.txt")
emfy = np.loadtxt("/u/ahankla/analysis_sheardynamo/data_reduced/xyazte_emf/xyazte_emfy_hgbLAIF_r64z4b4000znf_q15.txt")

bx1 = np.loadtxt("/u/ahankla/analysis_sheardynamo/data_reduced/xyazte_ft1/xyazte_ft_bx1_hgbLAIF_r64z4b4000znf_q15.txt")
by1 = np.loadtxt("/u/ahankla/analysis_sheardynamo/data_reduced/xyazte_ft1/xyazte_ft_by1_hgbLAIF_r64z4b4000znf_q15.txt")
Ex = pm.build_E_vector(emfx, bx1, by1)

Mx = pm.build_M_matrix(bx1, by1, True)
Mxslice = Mx[:,:,0]
print(Mxslice.shape)
Mxt0 = pm.build_M_matrix(bx1[0,:], by1[0,:], True)
print(Mxt0.shape)
print((Mxslice==np.squeeze(Mxt0)).all())

Cx = pm.get_transport_coefficients(emfx, bx1, by1, True)
print(np.mean(Cx, axis=1))
Cy = pm.get_transport_coefficients(emfy, bx1, by1, False)
print(np.mean(Cy, axis=1))

C = pm.get_all_transport_coefficients(emfx, emfy, bx1, by1)
print(C)
win = 50
plt.figure()
plt.plot(pm.moving_average(C["alpha_yy 1"], win), label="alpha_yy 1")
plt.plot(pm.moving_average(C["alpha_yy 2"], win), label="alpha_yy 2")
plt.axhline(np.mean(C["alpha_yy 1"]), color="black", linestyle='--')
plt.axhline(np.mean(C["alpha_yy 2"]), color="black", linestyle='--')
plt.legend()
plt.xlabel("Time")
plt.ylabel("alpha_yy")
#plt.savefig("alpha_yy.pdf")

plt.figure()
plt.plot(pm.moving_average(C["eta_xx 1"], win), label="eta_xx 1")
plt.plot(pm.moving_average(C["eta_xx 2"], win), label="eta_xx 2")
plt.axhline(np.mean(C["eta_xx 1"]), color="black", linestyle='--')
plt.axhline(np.mean(C["eta_xx 2"]), color="black", linestyle='--')
plt.legend()
plt.xlabel("Time")
plt.ylabel("eta_xx")
#plt.savefig("eta_xx.pdf")

plt.figure()
plt.plot(pm.moving_average(C["alpha_xy"], win))
plt.axhline(np.mean(C["alpha_xy"]), color="black", linestyle='--')
plt.xlabel("Time")
plt.ylabel("alpha_xy")
#plt.savefig("alpha_xy.pdf")

plt.figure()
plt.plot(pm.moving_average(C["eta_yx"], win))
plt.axhline(np.mean(C["eta_yx"]), color="black", linestyle='--')
plt.xlabel("Time")
plt.ylabel("eta_yx")
#plt.savefig("eta_xy.pdf")

plt.show()
