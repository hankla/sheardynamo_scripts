"""
Load in trials and run projection method.
"""
import numpy as np
import projection_method as pm
import matplotlib.pyplot as plt

qs = ["02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "14", "15"]
saved = True
C = dict()

for j in range(len(qs)-1, -1, -1):
    fname_c = "/home/hankla/analysis_sheardynamo/data_reduced/vate_transport_coefficients/vate_transport_coefficients_hgbLAIF_r64z4b4000znf_q"+qs[j]+".txt"
    Cq = np.loadtxt(fname_c)
    C[qs[j]] = Cq


## FIGURES
tstart = 500
mean_vals = np.zeros((len(list(C.keys())), 6))
keys = list(sorted(C.keys()))
for j in range(len(keys)):
    means = np.mean(C[keys[j]][tstart:, :], axis=0)
    mean_vals[j, :] = means


plt.figure()
plt.plot(mean_vals[:, 3], mean_vals[:, 2], "bo")
plt.axhline(0.0, ls="--", color="black")
plt.xlabel(r"$\eta_{xx}$ method 1")
plt.ylabel(r"$\alpha_{xy}$")
plt.tight_layout()

plt.figure()
plt.plot(mean_vals[:, 4], mean_vals[:, 2], "bo")
plt.axhline(0.0, ls="--", color="black")
plt.xlabel(r"$\eta_{xx}$ method 2")
plt.ylabel(r"$\alpha_{xy}$")
plt.tight_layout()

plt.figure()
plt.plot(mean_vals[5], mean_vals[0], "bo")
plt.axhline(0.0, ls="--", color="black")
plt.xlabel(r"$\eta_{yx}$")
plt.ylabel(r"$\alpha_{yy}$ method 1")
plt.tight_layout()

plt.figure()
plt.plot(mean_vals[5], mean_vals[1], "bo")
plt.axhline(0.0, ls="--", color="black")
plt.xlabel(r"$\eta_{yx}$")
plt.ylabel(r"$\alpha_{yy}$ method 2")
plt.tight_layout()

plt.show()

