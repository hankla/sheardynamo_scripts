"""
Load in trials and run projection method.
"""
import numpy as np
import projection_method_time as pmt

qs = ["03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "14", "15"]
#qs = ["111", "113", "115", "118"]
qs = ["05", "09", "12", "15"]
qs = ["05", "15"]

for j in range(len(qs)-1, -1, -1):
    fname_c = "/home/hankla/analysis_sheardynamo/data_reduced/vate_transport_coefficients/vate_transport_coefficients_"
    fend = "hgbLAIF_r64z4r2b4000znf_q"+qs[j]+".txt"

    # Load in file
    emfx = np.loadtxt("/home/hankla/analysis_sheardynamo/data_reduced/xyazte_emf/xyazte_emfx_hgbLAIF_r64z4r2b4000znf_q"+qs[j]+".txt")
    emfy = np.loadtxt("/home/hankla/analysis_sheardynamo/data_reduced/xyazte_emf/xyazte_emfy_hgbLAIF_r64z4r2b4000znf_q"+qs[j]+".txt")
    mean_bx = np.loadtxt("/home/hankla/analysis_sheardynamo/data_reduced/xyazte_mean_field/xyazte_bx_hgbLAIF_r64z4r2b4000znf_q"+qs[j]+".txt")
    mean_by = np.loadtxt("/home/hankla/analysis_sheardynamo/data_reduced/xyazte_mean_field/xyazte_by_hgbLAIF_r64z4r2b4000znf_q"+qs[j]+".txt")
    # Get coefficients!
    C_full, header_full, hl_full = pmt.get_transport_coefficients(mean_bx, mean_by, emfx, emfy, option=1)
    C_const, header_const, hl_const = pmt.get_transport_coefficients(mean_bx, mean_by, emfx, emfy, option=0)

    # Save to file
    np.savetxt(fname_c+"full_"+fend, np.transpose(C_full), header=hl_full)
    np.savetxt(fname_c+"const_"+fend, np.transpose(C_const), header=hl_const)
    print("Saved files: " + fname_c+fend)

