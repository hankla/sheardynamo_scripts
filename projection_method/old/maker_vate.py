"""
Load in trials and run projection method.
"""
import numpy as np
import projection_method as pm
import matplotlib.pyplot as plt

qs = ["02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "14", "15"]
qs=qs[:5]
saved = True
C = dict()

for j in range(len(qs)-1, -1, -1):
    fname_c = "/home/hankla/analysis_sheardynamo/data_reduced/vate_transport_coefficients/vate_transport_coefficients_hgbLAIF_r64z4b4000znf_q"+qs[j]+".txt"

    if saved:
        Cq = np.loadtxt(fname_c)
        C[qs[j]] = Cq
    else:
        emfx = np.loadtxt("/u/ahankla/analysis_sheardynamo/data_reduced/xyazte_emf/xyazte_emfx_hgbLAIF_r64z4b4000znf_q"+qs[j]+".txt")
        emfy = np.loadtxt("/u/ahankla/analysis_sheardynamo/data_reduced/xyazte_emf/xyazte_emfy_hgbLAIF_r64z4b4000znf_q"+qs[j]+".txt")
        mean_bx = np.loadtxt("/u/ahankla/analysis_sheardynamo/data_reduced/xyazte_mean_field/xyazte_bx_hgbLAIF_r64z4b4000znf_q"+qs[j]+".txt")
        mean_by = np.loadtxt("/u/ahankla/analysis_sheardynamo/data_reduced/xyazte_mean_field/xyazte_by_hgbLAIF_r64z4b4000znf_q"+qs[j]+".txt")
        # Get coefficients!
        C = pm.get_all_transport_coefficients(emfx, emfy, mean_bx, mean_by)
        np.savetxt(fname_c, np.transpose(C), header="alpha_yy alpha_xy eta_yx eta_xx")
        print("Saved " + fname_c)



key_list = np.array(C.keys())
num_keys = key_list.size
tmean_vals = np.zeros((num_keys, 6+1))
tstd_vals = np.zeros((num_keys, 6+1))
win = 100
tstart = 500
colors = iter(plt.cm.rainbow(np.linspace(0, 1, num_keys)))

for j in range(num_keys):
    if win > 1:
        for k in range(0, C[key_list[j]].shape[1]):
            data_smooth = pm.moving_average(C[key_list[j]][:, k], win)
            if 'data' in locals():
                data = np.vstack((data, data_smooth))
            else:
                data = data_smooth
        data = np.transpose(data)
    else:
        data = C[key_list[j]]
    mean_vals = np.mean(data[tstart:], axis=0)
    std_vals = np.std(data[tstart:], axis=0)
    del data
    qval = int(key_list[j])/10.
    tmean_vals[j, :] = np.insert(mean_vals, 0, qval)
    tstd_vals[j, :] = np.insert(std_vals, 0, qval)


ids = "hgbLAIF_r64z4b4000"
imgdir = "/home/hankla/analysis_sheardynamo/figures/vataqe_transport_coefficients/"
iname = "vataqe_"+ids+"_tstart{}".format(tstart)+"win{}".format(win)

plt.figure()
for key in sorted(C.keys()):
    col=next(colors)
    plt.plot(C[key][:, 1], c=col,label=key, ls="--")
plt.legend()



plt.show()
