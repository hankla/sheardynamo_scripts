"""
SUMMARY:
-Take the EMF files output by get_emf.py and get ready to fit to the
mean field equations via the projection method, as introduced in
Brandenburg & Sokoloff 2002 and implemented in Squire & Bhattacharjee 2016.

Options:
-full: no constraints set on coefficients.
-sb: constraints implemented: eta_yy = eta_xx, alpha_yy = alpha_xx, and
eta_xy = 0 = alpha_yx. These coefficients would otherwise fluctuate
wildly and pollute the other coefficients. As a result, instead of
Eqn. 3.2 in SB16, we use Eqn. 3.7, resulting in a different matrix M
for the Ex and Ey vectors, as detailed in the build_M_matrix function.

INPUTS:
OUTPUTS:

NOTES:
Can be run from any directory, given absolute path names.
Need to have run get_emf.py, get_mean_field.

Author: Lia Hankla
Date created: 2018-06-11
Modifications:
-2018-06-15: implement different options for constraints.
Next steps:
-Overwrite protection
-File open protection/error catching
-Do for first two modes instead of full data.
"""
import numpy as np


# ------------------------- FUNCTIONS ------------------
def get_volume_average(A, B):
    """
    Multiply quantities together and get volume
    average, i.e. <AB>.
    A and B must be the same size.
    Supposed to be one time slice
    """

    if A.shape != B.shape:
        print("""Error: A and B are not the same size.
        They are size {} and {}, respectively.""".format(A.shape, B.shape))
        return 0
    if A.ndim == 1:
        return np.mean(A*B)
    else:
        return np.mean(A*B, axis=1)


def build_E_vector(bx, by, emf1, emf2=None):
    """
    emf, b should be arrays with z and t data,
    formatted such that emfx[:,0] = time data of emfx at
    z = 0, emfx[0,:] = z-profile of exmf at time t = 0.

    OUTPUT:
    E(i) = ( <BxEx>, <ByEx>, <ExdzBx>, <ExdzBy>,
    <BxEy>, <ByEy>, <EydzBx>, <EydzBy>,
    Matrix E such that E[:,0] = the E vector E(i) at
    time = 0.
    """
    Ex1 = get_volume_average(emf1, bx)
    Ex2 = get_volume_average(emf1, by)
    Ex3 = get_volume_average(emf1, np.gradient(bx, axis=1))
    Ex4 = get_volume_average(emf1, np.gradient(by, axis=1))
    if emf2 is None:
        return np.array([Ex1, Ex2, Ex3, Ex4])
    else:
        Ey1 = get_volume_average(emf2, bx)
        Ey2 = get_volume_average(emf2, by)
        Ey3 = get_volume_average(emf2, np.gradient(bx, axis=1))
        Ey4 = get_volume_average(emf2, np.gradient(by, axis=1))

        return np.array([Ex1, Ex2, Ex3, Ex4, Ey1, Ey2, Ey3, Ey4])


def build_M_matrix(bx, by, option=1):
    """
    bx, by should be arrays with z and t data,
    formatted usch that bx[:,0] = time data of bx
    at z = 0, bx[0,:] = z-profile of bx at time t=0.
    Note that the matrices are not the same for Ex
    and Ey as they are originally in SB15...this
    is because of the constraints. Now we have:
    NOTE!!!!!!!! Different now
    E = M*C:
    BxEx  = * BxBx  BxBy  BxBy'  * alpha_yy
    ByEx  = * ByBx  ByBy  ByBy'  * alpha_xy
    Bx'Ex = * Bx'Bx Bx'By Bx'By' * eta_xx
    By'Ex = * By'Bx By'By By'By' *

    Ey = My*Cy:
    BxEy  =  * BxBy  BxBx'  BxBy' * alpha_yy
    ByEy     * ByBy  ByBx'  ByBy' * -eta_xx
    Bx'Ey    * Bx'By Bx'Bx' Bx'By'* eta_yx
    By'Ey    * By'By By'Bx' By'By'*

    """

    if bx.ndim == 1:
        time_steps = 1
        dzbx = np.gradient(bx)
        dzby = np.gradient(by)
    else:
        time_steps = bx.shape[0] # is time steps
        dzbx = np.gradient(bx, axis=1)
        dzby = np.gradient(by, axis=1)

    # Build common columns
    colx = np.zeros((4, time_steps))
    colx[0, :] = get_volume_average(bx, bx)
    colx[1, :] = get_volume_average(bx, by)
    colx[2, :] = get_volume_average(bx, dzbx)
    colx[3, :] = get_volume_average(bx, dzby)
    coly = np.zeros((4, time_steps))
    coly[0, :] = get_volume_average(by, bx)
    coly[1, :] = get_volume_average(by, by)
    coly[2, :] = get_volume_average(by, dzbx)
    coly[3, :] = get_volume_average(by, dzby)
    colyp = np.zeros((4, time_steps))
    colyp[0, :] = get_volume_average(dzby, bx)
    colyp[1, :] = get_volume_average(dzby, by)
    colyp[2, :] = get_volume_average(dzby, dzbx)
    colyp[3, :] = get_volume_average(dzby, dzby)
    colxp = np.zeros((4, time_steps))
    colxp[0, :] = get_volume_average(dzbx, bx)
    colxp[1, :] = get_volume_average(dzbx, by)
    colxp[2, :] = get_volume_average(dzbx, dzbx)
    colxp[3, :] = get_volume_average(dzbx, dzby)

    if option == 1:
        # For NO CONSTRAINTS option, just need to build one matrix
        M = np.zeros((4, 4, time_steps))
        # Construct full matrix
        M[:, 0, :] = colx
        M[:, 1, :] = coly
        M[:, 2, :] = colxp
        M[:, 3, :] = colyp

    elif option == 0:
        M = np.zeros((8, 4, time_steps))
        # Construct full matrix
        M[:4, 0, :] = colx
        M[:4, 1, :] = coly
        M[:4, 3, :] = colyp
        M[4:, 0, :] = coly
        M[4:, 2, :] = colyp
        M[4:, 3, :] = -colxp

    return M


def get_transport_coefficients(bx, by, emfx, emfy, option=1):
    timesteps = bx.shape[0]
    if option == 1:
        M = build_M_matrix(bx, by, option=1)
        Ex = build_E_vector(bx, by, emfx)
        Ey = build_E_vector(bx, by, emfy)
        C = np.zeros((8, timesteps))
    else:
        E = build_E_vector(bx, by, emfx, emfy)
        M = build_M_matrix(bx, by, option=0)
        C = np.zeros((4, timesteps))
    for t in range(timesteps):
        if option == 1:
            # LSQ Fit: only return the values, not the resids
            Cx = np.linalg.lstsq(M[:, :, t], Ex[:, t], rcond=None)[0]
            C[:4, t] = Cx
            Cy = np.linalg.lstsq(M[:, :, t], Ey[:, t], rcond=None)[0]
            C[4:, t] = Cy
            header = np.array([
                "$\alpha_{xx}$", "$\alpha_{xy}$", "$-\eta_{xy}$", "$\eta_{xx}$",
                "$\alpha_{yx}$", "$\alpha_{yy}$", "$-\eta_{yy}$", "$\eta_{yx}$"])
            header_list = ("alpha_xx alpha_xy -eta_xy eta_xx"
                           "alpha_yx alpha_yy -eta_yy eta_yx")
        else:
            Ci = np.linalg.lstsq(M[:, :, t], E[:, t], rcond=None)[0]
            C[:, t] = Ci
            header = np.array([
                "$\alpha_{yy}$", "$\alpha_{xy}$", "$\eta_{yx}$", "$\eta_{xx}$"
            ])
            header_list = ("alpha_yy alpha_xy eta_yx eta_xx")

    return C, header, header_list


def moving_average(a, n=3):
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n
