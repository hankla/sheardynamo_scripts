"""
SUMMARY:
-Take the EMF files output by get_emf.py and get ready to fit to the
mean field equations via the projection method, as introduced in
Brandenburg & Sokoloff 2002 and implemented in Squire & Bhattacharjee 2016.

Options:
-full: no constraints set on coefficients.
-sb: constraints implemented: eta_yy = eta_xx, alpha_yy = alpha_xx, and
eta_xy = 0 = alpha_yx. These coefficients would otherwise fluctuate
wildly and pollute the other coefficients. As a result, instead of
Eqn. 3.2 in SB16, we use Eqn. 3.7, resulting in a different matrix M
for the Ex and Ey vectors, as detailed in the build_M_matrix function.

INPUTS:
OUTPUTS:

NOTES:
Can be run from any directory, given absolute path names.
Need to have run get_emf.py, get_mean_field.

Author: Lia Hankla
Date created: 2018-06-11
Modifications:
-2018-06-15: implement different options for constraints.
Next steps:
-Overwrite protection
-File open protection/error catching
-Do for first two modes instead of full data.
"""
import numpy as np


# ------------------------- FUNCTIONS ------------------
def build_E_vector(bx, by, dzbx, dzby, emfx, emfy):
    """
    emf, b should be arrays with JUST t data,
    formatted such that emfx[0] = time data of emfx at
    t = 0, etc.

    OUTPUT:
    E(i) = ( BxEx(t=0), ByEx(t=0), ExdzBx, ExdzBy,
    BxEy, ByEy, EydzBx, EydzBy, BxEx(t=1), ...)
    Vector E for one height.
    """
    timesteps = bx.shape[0]

    Exx = np.multiply(emfx, bx)
    Exy = np.multiply(emfx, by)
    Exxp = np.multiply(emfx, dzbx)
    Exyp = np.multiply(emfx, dzby)
    Eyx = np.multiply(emfy, bx)
    Eyy = np.multiply(emfy, by)
    Eyxp = np.multiply(emfy, dzbx)
    Eyyp = np.multiply(emfy, dzby)
    E = np.array([])
    for t in range(timesteps):
        np.append(E, [Exx, Exy, Exxp, Exyp, Eyx, Eyy, Eyxp, Eyyp])
    return E


def build_M_matrix(bx, by, dzbx, dzby):
    """
    bx, by should be arrays with JUST t data,
    formatted usch that bx[:] = time data of bx
    at z = 0
    Note that the matrices are not the same for Ex
    and Ey as they are originally in SB15...this
    is because of the constraints. Now we have:


    """

    time_steps = bx.shape[0] # is time steps

    # Build common columns
    colx = np.zeros((4, time_steps))
    colx[0, :] = np.multiply(bx, bx)
    colx[1, :] = np.multiply(bx, by)
    colx[2, :] = np.multiply(bx, dzbx)
    colx[3, :] = np.multiply(bx, dzby)
    coly = np.zeros((4, time_steps))
    coly[0, :] = np.multiply(by, bx)
    coly[1, :] = np.multiply(by, by)
    coly[2, :] = np.multiply(by, dzbx)
    coly[3, :] = np.multiply(by, dzby)
    colyp = np.zeros((4, time_steps))
    colyp[0, :] = np.multiply(dzby, bx)
    colyp[1, :] = np.multiply(dzby, by)
    colyp[2, :] = np.multiply(dzby, dzbx)
    colyp[3, :] = np.multiply(dzby, dzby)
    colxp = np.zeros((4, time_steps))
    colxp[0, :] = np.multiply(dzbx, bx)
    colxp[1, :] = np.multiply(dzbx, by)
    colxp[2, :] = np.multiply(dzbx, dzbx)
    colxp[3, :] = np.multiply(dzbx, dzby)

    if option == 1:
        # For NO CONSTRAINTS option, just need to build one matrix
        M = np.zeros((4, 4, time_steps))
        # Construct full matrix
        M[:, 0, :] = colx
        M[:, 1, :] = coly
        M[:, 2, :] = colxp
        M[:, 3, :] = colyp

    elif option == 0:
        M = np.zeros((8, 4, time_steps))
        # Construct full matrix
        M[:4, 0, :] = colx
        M[:4, 1, :] = coly
        M[:4, 3, :] = colyp
        M[4:, 0, :] = coly
        M[4:, 2, :] = colyp
        M[4:, 3, :] = -colxp

    return M


def get_transport_coefficients(bx, by, emfx, emfy):
    zsteps = bx.shape[1]

    for z in range(zsteps):
        E = build_E_vector(bx, by, emfx, emfy)
        M = build_M_matrix(bx, by)
        C = np.zeros(4)
        Ci = np.linalg.lstsq(M[:, :, z], E[:, z], rcond=None)[0]
        C[:, z] = Ci
        header = np.array([
            "$\alpha_{yy}$", "$\alpha_{xy}$", "$\eta_{yx}$", "$\eta_{xx}$"
        ])
        header_list = ("alpha_yy alpha_xy eta_yx eta_xx")

    return C, header, header_list


def moving_average(a, n=3):
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n
