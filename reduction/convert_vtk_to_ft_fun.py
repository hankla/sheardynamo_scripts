import yt as yt
import numpy as np
import scipy.integrate
'''
SUMMARY:
-Read .vtk files that were output by an athena simulation and extract
the Fourier Transforms of the mean field.
INPUTS:
-res: tuple that specifies the resolution of the simulations. Important
for re-shaping yt data into an array.
-dname: location of the dataset. Must have ??? in order to read over
all the possible files.
-fname: the file that the time evolution will be written to, including
the directory.
written.
OUTPUTS:

NOTES:
run with "python convert_vtk_to_ft_fun.py". Data can be read
and plotted with e.g.
The script name indicates that the vtk files from athena output will be
converted into a python-friendly format
In general, "bar" in the variable name means the average over the x and y
directions.
Can be run from any directory, given absolute path names. 

Author: Lia Hankla
Date created: 2018-06-05 copied from working version of same name on isaac
Modifications:
2018-06-5: modified file paths for aida instead of isaac
Next steps:
-Overwrite protection
-File open protection/error catching'''

def convert_vtk(res, q, z, b, dpath):
    '''res: 1x3 list eg (128, 256, 128)
    . q = str, z = int,
    dpath eg "/disk1/hankla/data_work/trial_shear_data/hgbLAIF_r128z1b4000znf_q12_data/final_vtk_prim/hgbLAIF_"
    '''
    #----------------- INPUTS --------------------------
    sc = "z"+str(z)+"b"+str(b)+"znf_q"+q
    bs = "r"+str(res[0])+sc
    dname = dpath+bs+".????.vtk"
    fname = "/home/hankla/analysis_sheardynamo/data_reduced/xyate_ft12/xyate_ft12_hgbLAIF_"+bs+".txt"


    #----------------- FUNCTIONS ------------------------
    def from_yt(domain, field, res):
        '''Returns the field in an array with shape res (e.g. 32x64x32)'''
        return np.reshape(np.array(domain[field]), res)


    def get_bar_z(field):
        """
        The bar means to average over the x and y direction.
        z stipulates that the field is not reconstructed into a
        full matrix, as it would be in convert_vtk_to_python
        """
        fieldmean = np.mean(field, axis=(0, 1))
        return fieldmean


    def get_single_fourier_mode(data, wavelength):
        """
        Get a single Fourier mode. This route will
        numerically calculate
        datatilde(t)=\int data(z,t) exp(iz2\pi/wavelength) dz
        from z = 0 to z = L_z, where L_z is inferred from
        the number of entries in data.
        This is the amplitude of the Fourier mode with
        wavelength as per e.g. SSH16 Eq. 12.
        INPUTS:
        data = N_zones x #timesteps, data to be transformed.
        Should be only a
        function of the dimension given, i.e. Bbar_x(z,t), not
        Bbar_x(x,y,z,t). Should be in format data[1,:] = Bbar_x(z,t=0),
        data[2,:] = Bbar_x(z, t=1), etc..
        wavelength = wavelength of desired mode, in zones.
        OUTPUTS:
        Time-series of fourier amplitude of kth mode of data
        """

        if data.ndim == 1:
            N_zones = data.shape[0]
            time_steps = 1
        else:
            N_zones = data.shape[1]
            time_steps = data.shape[0]
        z = np.arange(0, N_zones)
        k = 2*np.pi/wavelength

        # Initialize time-series array
        single_mode_ft = np.zeros(time_steps)

        for t in np.arange(time_steps):
            if data.ndim == 1:
                data_zslice = data
            else:
                data_zslice = data[t, :]
            integrand = data_zslice*np.exp(-1j*k*z)
            # Do the integral. Take only the real part
            single_mode_ft[t] = np.real(scipy.integrate.simps(integrand, z))

        return single_mode_ft


    #--------------- INITIALIZATION -----------------------
    time = []; Bx1 = []; Bx2 = []; By1 = []; By2 = []
    N_zones = res[2]*z
    ts = yt.load(dname)

    #------------- GET TIME DATA ---------------------
    count = 0
    print("Loading " + dname + " data")
    for ds in ts: # for dataset in timeseries
        print("Fetching data " + str(count)); count += 1
        time.append(ds.current_time)
        ad = ds.all_data()
        Bx = from_yt(ad, "cell_centered_B_x", res)
        By = from_yt(ad, "cell_centered_B_y", res)
        Bxbar = get_bar_z(Bx)
        Bybar = get_bar_z(By)

        N_zones = Bxbar.size

        # Get amplitudes. These should be simply numbers, not arrays
        bx1 = get_single_fourier_mode(Bxbar, N_zones)
        bx2 = get_single_fourier_mode(Bxbar, N_zones/2) # or is it N_zones - 1 ? XX
        by1 = get_single_fourier_mode(Bybar, N_zones)
        by2 = get_single_fourier_mode(Bybar, N_zones/2)

        Bx1.append(bx1)
        Bx2.append(bx2)
        By1.append(by1)
        By2.append(by2)


    #Create full matrix
    full = np.matrix(np.transpose([time, Bx1, Bx2, By1, By2]))
    h1 = "time bx1 bx2 by1 by2"
    np.savetxt(fname, full, '%e', header=h1)
    print("Finished saving "+fname)
    return full

z = "1"
qs = ["03","04","05","06","07","09","10","11", "12", "13", "14", "15"]
#qs = ["05", "09", "12", "15"]

res64 = (64, 128, 64)
res128 = (128, 256, 128)
res32 = (32, 64, 32)

for j in range(len(qs)-1,-1,-1):
    #dpr128 = "/disk1/hankla/data_work/trial_shear_data/hgbLAIF_r128z"+z+"b4000znf_q"+qs[j]+"_data/final_vtk_prim/hgbLAIF_"
    #dpr32 = "/home/hankla/data_work/trial_shear_data/hgbLAIF_r32z"+z+"b4000znf_q"+qs[j]+"_data/final_vtk_prim/hgbLAIF_"
    dpr64 = "/run/media/hankla/Seagate Backup Plus Drive/work_fulbright2017-8/data_work/trial_shear_data/hgbLAIF_r64z"+z+"b4000znf_q"+qs[j]+"_data/final_vtk_prim/hgbLAIF_"
    convert_vtk(res64, qs[j], int(z), 4000, dpr64)
    

##qs = ["01","02","03","04","05","06","07","08","09","10","11"]
#qs = ["15"] # , "13", "14", "15"]
#z = "1"
#
#res = (64, 128, 64)
#
#for j in range(len(qs)):
    #dpr64 = "/isaac/ptmp/gc/ahankla/work_data/trial_shear_data/test_data/hgbLAIF_"
    #full = convert_vtk(res, qs[j], int(z), 4000, dpr64)
#
