'''
SUMMARY:
-Read .vtk files that were output by an athena simulation and extract
the best timelag between the mean field in the x and y direction, as
per SSH16 Table 1 and Sec. 4.
INPUTS:
-res: tuple that specifies the resolution of the simulations. Important
for re-shaping yt data into an array.
-dname: location of the dataset. Must have ??? in order to read over
all the possible files.
-fname: the file that the time evolution will be written to, including
the directory.
written.
OUTPUTS:

NOTES:
run with "python get_timelag.py". Data can be read
and plotted with e.g.
The script name indicates that the vtk files from athena output will be
converted into a python-friendly format
In general, "bar" in the variable name means the average over the x and y
directions.
Can be run from any directory, given absolute path names.

Author: Lia Hankla
Date created: 2018-06-02 from convert_vtk_to_py_ssh16_fun.py
Modifications:
Next steps:
-Overwrite protection
-File open protection/error catching'''
import yt as yt
import numpy as np


def convert_vtk(res, q, z, b, dpath, Tcycle, save_fourier=False):
    '''res: 1x3 list eg (128, 256, 128)
    . q = str, z = int,
    dpath eg "/disk1/hankla/data_work/trial_shear_data/hgbLAIF_r128z1b4000znf_q12_data/final_vtk_prim/hgbLAIF_"
    '''
    #----------------- INPUTS --------------------------
    sc = "z"+str(z)+"b"+str(b)+"znf_q"+q
    bs = "r"+str(res[0])+sc
    dname = dpath+bs+".????.vtk"
    fname_lag = "/home/hankla/analysis_sheardynamo/data_reduced/xyaze_timelag/xyaze_timelag_hgbLAIF_"+bs+".txt"
    fname_bx1 = "/home/hankla/analysis_sheardynamo/data_reduced/xyazte_ft1/xyazte_ft_bx1_hgbLAIF_"+bs+".txt"
    fname_by1 = "/home/hankla/analysis_sheardynamo/data_reduced/xyazte_ft1/xyazte_ft_by1_hgbLAIF_"+bs+".txt"


    #----------------- FUNCTIONS ------------------------
    def from_yt(domain, field, res):
        '''Returns the field in an array with shape res (e.g. 32x64x32)'''
        return np.reshape(np.array(domain[field]), res)


    def get_bar_z(field):
        """
        The bar means to average over the x and y direction.
        z stipulates that the field is not reconstructed into a
        full matrix, as it would be in convert_vtk_to_python
        """
        fieldmean = np.mean(field, axis=(0, 1))
        return fieldmean


    def get_time_lag(bx, by, Tcycle, save_fourier):
        """
        Get the time lag between the mean field in the
        x and y directions by calculating the cross-correlation
        between the two time series at the same height z and
        taking the lag with the most negative correlation,
        as per SSH16 Sec. 4
        INPUTS:
        bx: mean field in the x direction,
        dependent on z and t, formatted such that
        bx[0] = bx(t=0,z), a list. bx[1] = bx(t = 1, z).
        bx[0][5] = bx(t=0, z=5)
        by: mean field in the y direction,
        dependent on z and t, formatted the
        same as bx.
        NOTE: Constrain time_lag to be between 0 and Tcycle.
        To get Tcycle, need to have run fig_psd_... and
        created cycle_period.txt output.
        NOTE: assume that this cycle_period.txt file has the
        same number of entries and is in the correct order!!
        OUTPUTS:
        Array of timelags at each height
        TO DO:
        Read cycle_period.txt first/second columns and match
        """
        print("Tcycle: "+str(Tcycle))
        # time_steps = len(bx)
        N_zones = len(bx[0])

        bx = np.array(bx)
        by = np.array(by)
        time_lag = []

        for z in np.arange(N_zones):
            # Get full correlation data
            c = np.correlate(bx[:, z], by[:, z], mode=2)
            Nx = bx[:, z].size
            # Cut window to be no greater than 1.5Tcycle
            # otherwise largest will be obv. the entire time
            maxlag = int(1.5*Tcycle*2*np.pi) # convert back to code time
            c = c[Nx - 1 - maxlag:Nx + maxlag]
            # Find index of most positive correlation
            # (NOTE: SSH16 finds most negative...but
            # then subtracts from pi.)
            maxpos_ind = c.argmax()
            # Create array of lags
            lags = np.arange(-maxlag, maxlag + 1, 1)
            # Get lag of maximum correlation
            maxpos_lag = lags[maxpos_ind]

            time_lag.append(maxpos_lag)
        return time_lag


    #--------------- INITIALIZATION -----------------------
    Bx1 = []; By1 = []
    ts = yt.load(dname)

    #------------- GET TIME DATA ---------------------
    count = 0
    print("Loading " + dname + " data")
    for ds in ts: # for dataset in timeseries
        print("Fetching data " + str(count)); count += 1
        ad = ds.all_data()
        Bx = from_yt(ad, "cell_centered_B_x", res)
        By = from_yt(ad, "cell_centered_B_y", res)
        Bxbar = get_bar_z(Bx)
        Bybar = get_bar_z(By)

        Bx1.append(Bxbar)
        By1.append(Bybar)

    # ------------- GET TIME LAG ---------------------
    full_lag = get_time_lag(Bx1, By1, Tcycle, save_fourier)
    # Save lags to file
    header_lag = "Time lag"
    np.savetxt(fname_lag, full_lag, '%e', header=header_lag)
    if save_fourier:
        # Save Fourier modes to file
        np.savetxt(fname_bx1, Bx1)
        np.savetxt(fname_by1, By1)
    print("Finished saving "+fname_lag)
    return full_lag

z = "1"
qs64 = ["03","04","05","06","07","09","10","11", "12", "13", "14", "15"]
qs = ["05", "09", "12", "15"]

res128 = (128, 256, 128)
res32 = (32, 64, 32)
res64 = (64, 128, 64)

save_fourier = True
fname_cycles64 = "/home/hankla/analysis_sheardynamo/data_reduced/cycle_period/cycle_period64.txt"
cycle_data64 = np.loadtxt(fname_cycles64)
Tcycle64 = cycle_data64[:,2]
fname_cycles128 = "/home/hankla/analysis_sheardynamo/data_reduced/cycle_period/cycle_period128.txt"
cycle_data128 = np.loadtxt(fname_cycles128)
Tcycle128 = cycle_data128[:,2]
fname_cycles32 = "/home/hankla/analysis_sheardynamo/data_reduced/cycle_period/cycle_period32.txt"
cycle_data32 = np.loadtxt(fname_cycles32)
Tcycle32 = cycle_data32[:,2]

for j in range(len(qs64)-1,-1,-1):
    dpr64 = "/run/media/hankla/Seagate Backup Plus Drive/work_fulbright2017-8/data_work/trial_shear_data/hgbLAIF_r64z"+z+"b4000znf_q"+qs64[j]+"_data/final_vtk_prim/hgbLAIF_"
    convert_vtk(res64, qs64[j], int(z), 4000, dpr64, Tcycle64[j], save_fourier)

for j in range(len(qs)-1,-1,-1):
    dpr128 = "/disk1/hankla/data_work/trial_shear_data/hgbLAIF_r128z"+z+"b4000znf_q"+qs[j]+"_data/final_vtk_prim/hgbLAIF_"
    dpr32 = "/home/hankla/data_work/trial_shear_data/hgbLAIF_r32z"+z+"b4000znf_q"+qs[j]+"_data/final_vtk_prim/hgbLAIF_"
    convert_vtk(res128, qs[j], int(z), 4000, dpr128, Tcycle128[j], save_fourier)
    convert_vtk(res32, qs[j], int(z), 4000, dpr32, Tcycle32[j], save_fourier)




## --------- TEST CASE ---------------------
#qs = ["15"] # , "13", "14", "15"]
#z = "1"
#res = (64, 128, 64)
#for j in range(len(qs)):
    #dpr64 = "/isaac/ptmp/gc/ahankla/work_data/trial_shear_data/test_data/hgbLAIF_"
    #full = convert_vtk(res, qs[j], int(z), 4000, dpr64, 600)
#print(np.array(full).shape)
#print(full)
#print(np.mean(full)/(2*np.pi))
