import yt as yt
import numpy as np
"""
SUMMARY:
-Read .vtk files that were output by an athena simulation and extract
the time evolution of the total, mean, and turbulent magnetic energy,
azimuthal field energy, and Maxwell stresses.
INPUTS:
-res: tuple that specifies the resolution of the simulations. Important
for re-shaping yt data into an array.
-dname: location of the dataset. Must have ??? in order to read over
all the possible files.
-fname: the file that the time evolution will be written to, including
the directory.
-fbname: the file to which the butterfly diagram information will be
written.
OUTPUTS:
-the file fname with the format
time totME turbME meanME totMx turbMx meanMx totByME turbByME meanByME
-optionally output snapshots of the turbulent/mean/total magnetic
energy, etc. Controlled by XXXX (not yet implemented)

NOTES:
run with "python convert_vtk_to_py_tevab_ssh16.py". Data can be read
and plotted with e.g. rescompare_qz_r.py.
The script name indicates that the vtk files from athena output will be
converted into a python-friendly format similar to that of the time
evolution volume averaged information explored in Shi, Stone, Huang 2016
as well as the butterfly diagram.
In general, capital "B" means total, lowercase "b" means turbulent,
"bar" in the variable name means the average over the x and y
directions. "a" at the end signales an average over the entire volume.
Can be run from any directory when absolute path names are given
(which is the default).

Author: Lia Hankla
Date created: 2018-04-06
Modifications:
-2018-04-09: renamed.
Next steps:
-Output snapshots of the turbulent/mean/total magnetic energy/etc
-Write script to easily read, compare resolution stress levels
-Overwrite protection
-File open protection/error catching
"""
def convert_vtk(res, q, z, b, dpath):
    """res: 1x3 list eg (128, 256, 128)
    . q = str, z = int,
    dpath eg "/disk1/hankla/data_work/trial_shear_data/
        hgbLAIF_r128z1b4000znf_q12_data/final_vtk_prim/hgbLAIF_"
    """
    #----------------- INPUTS --------------------------
    sc = "z"+str(z)+"b"+str(b)+"znf_q"+q
    bs = "r"+str(res[0])+sc
    dname = dpath+bs+".????.vtk"
    fname = "/u/ahankla/analysis_work/trial_shear_analysis/temt_files/TE_MEMyMx_hgbLAIF_"+bs+".txt"
    fbname = "/u/ahankla/analysis_work/trial_shear_analysis/butterfly_files/butterfly_hgbLAIF_"+bs+".txt"

    #----------------- FUNCTIONS ------------------------
    def from_yt(domain, field, res):
        '''Returns the field in an array with shape res (e.g. 32x64x32)'''
        return np.reshape(np.array(domain[field]), res)

    def get_bar(field, res):
        '''The bar means to average over the x and y direction
        but this function also reconstructs into a full matrix
        with dimensions of res. '''
        fieldbar = np.zeros(res)
        fieldmean = np.mean(field,axis=(0,1))
        for i in range(res[0]):
            for j in range(res[1]):
                fieldbar[i,j,:] = fieldmean
        return fieldbar

    def get_ME(bx,by,bz):
        '''Get the magnetic energy...note that it is NOT
        divided by 8pi yet!'''
        return np.mean(np.square(bx)+np.square(by)+np.square(bz))


    def get_Mx(bx,by):
        '''Get the Maxwell stress...note that it is NOT
        divided by 4pi yet!'''
        return np.mean(-np.multiply(bx,by))

    #--------------- INITIALIZATION -----------------------
    f = 1
    time = [];
    B2a = []; Bybar2a = []; by2a = []; bxbya = []; Bbar2a = []; BxbarBybara = []; BxBya = []; b2a = []; By2a =[];

    ts = yt.load(dname)
    t = 0

    #------------- GET TIME DATA ---------------------
    count = 0
    print("Loading "+dname+" data")
    for ds in ts:
        print("Fetching data "+str(count)); count+=1
        time.append(ds.current_time)
        ad = ds.all_data()
        Bx = from_yt(ad,"cell_centered_B_x",res)
        By = from_yt(ad,"cell_centered_B_y",res)
        Bz = from_yt(ad,"cell_centered_B_z",res)
        Bxbar = get_bar(Bx,res)
        Bybar = get_bar(By,res)
        Bzbar = get_bar(Bz,res)
        bx = Bx - Bxbar
        by = By - Bybar
        bz = Bz - Bzbar

        if 'Byzt' in locals():
            Byzt = np.vstack((Byzt,Bybar[0,0,:])); 
        else:
            Byzt = np.matrix(Bybar[0,0,:])
        t+=1
        #Magnetic Energies
        B2a.append(get_ME(Bx,By,Bz)) #total magnetic energy
        b2a.append(get_ME(bx,by,bz)) #turbulent magnetic energy
        Bbar2a.append(get_ME(Bxbar,Bybar,Bzbar)) #mean field magnetic energy

        #Maxwell Stresses
        BxBya.append(get_Mx(Bx,By))
        bxbya.append(get_Mx(bx,by))
        BxbarBybara.append(get_Mx(Bxbar,Bybar))

        #Azimuthal Magnetic Field Energy
        By2a.append(np.mean(np.square(By)))
        by2a.append(np.mean(np.square(by)))
        Bybar2a.append(np.mean(np.square(Bybar)))

        #Make some plots
        #get time stamp from sim. If it's one we want, then plot tu

    #Create full matrix
    full=np.matrix(np.transpose([time,B2a,b2a,Bbar2a,BxBya,bxbya,BxbarBybara,By2a,by2a,Bybar2a]))
    h1="time B2a b2a Bbar2a BxBya bxbya BxbarBybara By2a by2a Bybar2a"
    np.savetxt(fname,full,'%e',header=h1)
    np.savetxt(fbname,Byzt,'%e')
    print("Finished saving "+fname)

#qs = ["01","02","03","04","05","06","07","08","09","10","11"]
qs = ["12", "13", "14", "15"]
z = "4"

res = (64, 128, 256)

for j in range(len(qs)):
    dpr64 = "/isaac/ptmp/gc/ahankla/work_data/trial_shear_data/hgbLAIF_r64z"+z+"b4000znf_q"+qs[j]+"_data/final_vtk_prim/hgbLAIF_"
    convert_vtk(res, qs[j], int(z), 4000, dpr64)
