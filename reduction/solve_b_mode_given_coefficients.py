import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import odeint


def b_field_system(state, t, q, alphayy, alphaxy, etayy, etayx):
    bx, by = state
    L_z = 4
    k = 2*np.pi/L_z

    # Linearly interpolate coefficients
    t_low = int(np.floor(t))
    t_high = int(np.ceil(t))
    if t_low == t_high:
        alphayyi = alphayy[t_low]
        alphaxyi = alphaxy[t_low]
        etayyi = etayy[t_low]
        etayxi = etayx[t_low]
    else:
        if t_high >= alphayy.size:
            t_high = alphayy.size-1
            alphayyi = alphayy[t_high]
            alphaxyi = alphaxy[t_high]
            etayyi = etayy[t_high]
            etayxi = etayx[t_high]
        else:
            alphayyi = (t-t_low)*(alphayy[t_high]-alphayy[t_low])/(t_high-t_low) + alphayy[t_low]
            alphaxyi = (t-t_low)*(alphaxy[t_high]-alphaxy[t_low])/(t_high-t_low) + alphaxy[t_low]
            etayyi = (t-t_low)*(etayy[t_high]-etayy[t_low])/(t_high-t_low) + etayy[t_low]
            etayxi = (t-t_low)*(etayx[t_high]-etayx[t_low])/(t_high-t_low) + etayx[t_low]
    dbx = -etayyi*k**2*bx+(etayxi*k**2-1j*k*alphayyi)*by
    dby = (-q+1j*k*alphayyi)*bx+(-etayyi*k**2+1j*k*alphaxyi)*by

    return [dbx, dby]


def simple_sys(state, t, a, b):
    x = state[0]
    print(t)
    # Linearly interpolate ai
    t_low = int(np.floor(t))
    t_high = int(np.ceil(t))
    print("t_low: {}".format(t_low))
    print(t_high)
    print("a[tlow]: {}".format(a[t_low]))
    if t_low == t_high:
        ai = a[t_low]
    else:
        if t_high >= a.size:
            t_high = a.size-1
            ai = a[t_high]
            print("new t_high: {}".format(t_high))
        else:
            ai = (t-t_low)*(a[t_high]-a[t_low])/(t_high-t_low) + a[t_low]
    print("Interpolated ai: {}".format(ai))
    print("b: {}".format(b))

    return -x+ai*b



tmax = 5
trange = np.arange(0, tmax, 1)
t = np.arange(0, tmax, 1)
a = (tmax/2. - t)/(tmax/2.)
b = tmax*5.
x0 = 0.8

init_state = (x0, )
args = (a, b)

#y = odeint(simple_sys, x0, t, args)
#print(a)

fname_const = "/home/hankla/analysis_sheardynamo/data_reduced/vate_transport_coefficients/vate_transport_coefficients_const_hgbLAIF_r64z4b4000znf_q15.txt"
C_const = np.loadtxt(fname_const)
q = 1.5
alphayy = C_const[0, :]
alphaxy = C_const[1, :]
etayx = C_const[2, :]
etaxx = C_const[3, :]
init_state = [1e-9, 2e-9]
args = (q, alphayy, alphaxy, etayx, etaxx)
num_steps = alphayy.size
t = np.linspace(0, num_steps/(2*np.pi*10), num_steps)
b_states = odeint(b_field_system, init_state, t, args)
plt.figure()
plt.plot(b_states[0, :], b_states[1, :])
plt.plot(b_states[0, :], b_states[2, :])
print(b_states.size)
plt.show()
