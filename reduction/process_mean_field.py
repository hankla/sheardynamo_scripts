"""
A la GP15 Sec. 3 p. 8, process mean field to make butterfly cycle more apparent.
"""

import numpy as np
import matplotlib.pyplot as plt

def enhance_cycle_amplitude(bdata):
    """
    Given by data, normalize slice at each t by maximum
    in order to enhance cycles.
    """
    time_steps = bdata.shape[0]
    bdata_processed = np.zeros(bdata.shape)
    for t in range(time_steps):
        zslice = bdata[t, :]
        zmax = np.max(np.abs(zslice))
        bdata_processed[t, :] = zslice/zmax if zmax > 0 else zslice
    return bdata_processed

def get_largest_fourier_modes(data):
    """
    data must be formatted such that the last axis
    is the one over which to take the fft (default setting),
    i.e. data[0,:] = z-data at time t, data[:, 0] = time data
    at z = 0.
    """
    spectrum = np.fft.rfft(data) #default setting: fft over last axis.
    freqs = np.fft.rfftfreq(data.shape[-1])
    magnitude = np.abs(spectrum)
    phase = np.angle(spectrum)
    # Take largest two mode data
    # Note: 0th entry is zero frequency term, sum of signal
    if data.ndim != 1:
        largest_mode_mag = magnitude[:, 1]
        largest_mode_phase = phase[:, 1]
        second_mode_mag = magnitude[:, 2]
        second_mode_phase = phase[:, 2]
    else:
        largest_mode_mag = magnitude[1]
        largest_mode_phase = phase[1]
        second_mode_mag = magnitude[2]
        second_mode_phase = phase[2]
    # Normalize magnitudes
    N_zones = data.shape[1]
    largest_mode_mag *= 2/N_zones
    second_mode_mag *= 2/N_zones
    return np.array([largest_mode_mag, largest_mode_phase, second_mode_mag, second_mode_phase])


# Set-up
idstr = "hgbLAIF"
rval = "64"; zval = "4"; bval = "4000"; qval = "15"
bydata = np.loadtxt("/home/hankla/analysis_sheardynamo/data_reduced/xyazte_mean_field/xyazte_by_"+idstr+"_r"+rval+"z"+zval+"b"+bval+"znf_q"+qval+".txt")
bydata_enhanced = enhance_cycle_amplitude(bydata)

# Visually compare enhanced and unprocessed
f = 1
time_steps = bydata.shape[0]
extent_ = [0, time_steps/(2*np.pi), -int(zval)/2, int(zval)/2]
aspect_ = "auto"
fig, axes = plt.subplots(2, 1, num=f); f += 1
ax1 = axes[0]; ax2 = axes[1]
cax1 = ax1.imshow(np.transpose(bydata), extent=extent_, aspect=aspect_)
ax1.set_title("Original Azimuthal B")
ax1.set_ylabel("z [H]")
cax2 = ax2.imshow(np.transpose(bydata_enhanced), extent=extent_, aspect=aspect_)
ax2.set_title("Enhanced Azimuthal B")
ax2.set_xlabel("Time (Orbits)")
ax2.set_ylabel("z [H]")
plt.tight_layout()
cb = fig.colorbar(cax1, ax=axes.ravel().tolist())
cb.set_label(idstr+"_r"+rval+"z"+zval+"b"+bval+"_q"+qval)
# plt.show()

qs = ["02","03","04","05","06","07","08","09","10","11", "12", "13", "14", "15"]

for j in range(len(qs)-1, -1, -1):
    datax = np.loadtxt("/home/hankla/analysis_sheardynamo/data_reduced/xyazte_mean_field/xyazte_bx_hgbLAIF_r64z4b4000znf_q"+qs[j]+".txt")
    outnamex = "/home/hankla/analysis_sheardynamo/data_reduced/xyate_ft12/xyate_ft12_bx_processed_hgbLAIF_r64z4b4000znf_q"+qs[j]+".txt"
    datay = np.loadtxt("/home/hankla/analysis_sheardynamo/data_reduced/xyazte_mean_field/xyazte_by_hgbLAIF_r64z4b4000znf_q"+qs[j]+".txt")
    outnamey = "/home/hankla/analysis_sheardynamo/data_reduced/xyate_ft12/xyate_ft12_by_processed_hgbLAIF_r64z4b4000znf_q"+qs[j]+".txt"

    ft_bx = get_largest_fourier_modes(enhance_cycle_amplitude(datax))
    ft_by = get_largest_fourier_modes(enhance_cycle_amplitude(datay))

    np.savetxt(outnamex, np.transpose(ft_bx), header="bx1_mag bx1_phase bx2_mag bx2_phase")
    np.savetxt(outnamey, np.transpose(ft_by), header="by1_mag by1_phase by2_mag by2_phase")
    print("Saved "+outnamey)
