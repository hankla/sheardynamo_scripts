'''
SUMMARY:
-Read .vtk files that were output by an athena simulation and extract
the EMF in the x and y directions as a function of z and t, as
needed for the projection method of SB16 (and Brandenburg & Sokoloff 02)
INPUTS:
-res: tuple that specifies the resolution of the simulations. Important
for re-shaping yt data into an array.
-dname: location of the dataset. Must have ??? in order to read over
all the possible files.
-fname: the file that the time evolution will be written to, including
the directory.
written.
OUTPUTS:

NOTES:
run with "python get_timelag.py". Data can be read
and plotted with e.g.
The script name indicates that the vtk files from athena output will be
converted into a python-friendly format
In general, "bar" in the variable name means the average over the x and y
directions.
Can be run from any directory, given absolute path names.

Author: Lia Hankla
Date created: 2018-06-02 from convert_vtk_to_py_ssh16_fun.py
Modifications:
Next steps:
-Overwrite protection
-File open protection/error catching'''
import yt as yt
import numpy as np


def convert_vtk(res, q, z, b, dpath):
    '''res: 1x3 list eg (128, 256, 128)
    . q = str, z = int,
    dpath eg "/disk1/hankla/data_work/trial_shear_data/hgbLAIF_r128z1b4000znf_q12_data/final_vtk_prim/hgbLAIF_"
    '''
    #----------------- INPUTS --------------------------
    sc = "z"+str(z)+"b"+str(b)+"znf_q"+q
    bs = "r"+str(res[0])+sc
    dname = dpath+bs+".????.vtk"
    fname_emfx = "/home/hankla/analysis_sheardynamo/data_reduced/xyazte_emf/xyazte_emfx_hgbLAIF_"+bs+".txt"
    fname_emfy = "/home/hankla/analysis_sheardynamo/data_reduced/xyazte_emf/xyazte_emfy_hgbLAIF_"+bs+".txt"

    fname_Bx = "/home/hankla/analysis_sheardynamo/data_reduced/xyazte_mean_field/xyazte_bx_hgbLAIF_" + bs + ".txt"
    fname_By = "/home/hankla/analysis_sheardynamo/data_reduced/xyazte_mean_field/xyazte_by_hgbLAIF_" + bs + ".txt"

    #----------------- FUNCTIONS ------------------------
    def from_yt(domain, field, res):
        '''Returns the field in an array with shape res (e.g. 32x64x32)'''
        return np.reshape(np.array(domain[field]), res)


    def get_bar(field, res):
        '''The bar means to average over the x and y direction
        but this function also reconstructs into a full matrix
        with dimensions of res. '''
        fieldbar = np.zeros(res)
        fieldmean = np.mean(field,axis=(0,1))
        for i in range(res[0]):
            for j in range(res[1]):
                fieldbar[i,j,:] = fieldmean
        return fieldbar


    def get_bar_z(field):
        """
        The bar means to average over the x and y direction.
        z stipulates that the field is not reconstructed into a
        full matrix, as it would be in convert_vtk_to_python
        """
        fieldmean = np.mean(field, axis=(0, 1))
        return fieldmean


    #--------------- INITIALIZATION -----------------------
    emfx = []; emfy = []; mean_field_x = []; mean_field_y = []
    ts = yt.load(dname)
    fbx = open(fname_Bx, 'w')
    fby = open(fname_By, 'w')
    femfx = open(fname_emfx, 'w')
    femfy = open(fname_emfy, 'w')

    #------------- GET TIME DATA ---------------------
    count = 0
    print("Loading " + dname + " data")
    for ds in ts: # for dataset in timeseries
        print("Fetching data " + str(count)); count += 1
        ad = ds.all_data()
        Bx = from_yt(ad, "cell_centered_B_x", res)
        By = from_yt(ad, "cell_centered_B_y", res)
        Bz = from_yt(ad, "cell_centered_B_z", res)

        Vx = from_yt(ad, "velocity_x", res)
        Vy = from_yt(ad, "velocity_y", res)
        Vz = from_yt(ad, "velocity_z", res)

        # Mean fields
        Bxbar = get_bar(Bx, res)
        Bybar = get_bar(By, res)
        Bzbar = get_bar(Bz, res)

        # Turbulent fields
        bx = Bx - Bxbar
        by = By - Bybar
        bz = Bz - Bzbar

        vx = Vx - get_bar(Vx, res)
        vy = Vy - get_bar(Vy, res)
        vz = Vz - get_bar(Vz, res)

        # Calculate cross-products
        emfx_full = vy*bz - vz*by
        emfy_full = vz*bx - vx*bz

        # Average over x and y directions
        emfx_avg = get_bar_z(emfx_full)
        emfy_avg = get_bar_z(emfy_full)
        bx_avg = get_bar_z(Bx)
        by_avg = get_bar_z(By)
        for i in range(emfx_avg.size):
            fbx.write(str(bx_avg[i])+" ")
            fby.write(str(by_avg[i]) + " ")
            femfx.write(str(emfx_avg[i]) + " ")
            femfy.write(str(emfy_avg[i]) + " ")
        fbx.write("\n")
        fby.write("\n")
        femfx.write("\n")
        femfy.write("\n")

    # Close all files
    fbx.close()
    fby.close()
    femfx.close()
    femfy.close()
    print("Finished saving "+fname_emfx)
    return emfx, emfy


z = "1"
qs64 = ["03","04","05","06","07","09","10","11", "12", "13", "14", "15"]
qs64 = ["15"]
qs = ["05", "09", "12", "15"]

res128 = (128, 256, 128)
res32 = (32, 64, 32)
res64 = (64, 128, 64)

for j in range(len(qs64)-1,-1,-1):
    dpr64 = "/run/media/hankla/Seagate Backup Plus Drive/work_fulbright2017-8/data_work/trial_shear_data/hgbLAIF_r64z"+z+"b4000znf_q"+qs64[j]+"_data/final_vtk_prim/hgbLAIF_"
    convert_vtk(res64, qs64[j], int(z), 4000, dpr64)

for j in range(len(qs)-1,-1,-1):
    dpr128 = "/disk1/hankla/data_work/trial_shear_data/hgbLAIF_r128z"+z+"b4000znf_q"+qs[j]+"_data/final_vtk_prim/hgbLAIF_"
    dpr32 = "/home/hankla/data_work/trial_shear_data/hgbLAIF_r32z"+z+"b4000znf_q"+qs[j]+"_data/final_vtk_prim/hgbLAIF_"
    convert_vtk(res128, qs[j], int(z), 4000, dpr128)
    convert_vtk(res32, qs[j], int(z), 4000, dpr32)


## --------- TEST CASE ---------------------
#qs = ["15"] # , "13", "14", "15"]
#z = "1"
#res = (64, 128, 64)
#for j in range(len(qs)):
    #dpr64 = "/isaac/ptmp/gc/ahankla/work_data/trial_shear_data/test_data/hgbLAIF_"
    #convert_vtk(res, qs[j], int(z), 4000, dpr64)
#print(np.array(full).shape)
#print(full)
#print(np.mean(full)/(2*np.pi))
