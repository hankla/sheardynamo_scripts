'''
SUMMARY:
-Read .vtk files that were output by an athena simulation and extract
the Fourier Transforms of the mean field.
INPUTS:
-res: tuple that specifies the resolution of the simulations. Important
for re-shaping yt data into an array.
-dname: location of the dataset. Must have ??? in order to read over
all the possible files.
-fname: the file that the time evolution will be written to, including
the directory.
written.
OUTPUTS:

NOTES:
run with "python convert_vtk_to_ft_fun.py". Data can be read
and plotted with e.g.
The script name indicates that the vtk files from athena output will be
converted into a python-friendly format
In general, "bar" in the variable name means the average over the x and y
directions.
Can be run from any directory, given absolute path names.

Author: Lia Hankla
Date created: 2018-06-02 from convert_vtk_to_py_ssh16_fun.py
Modifications:
-2018-06-05: updated get_single_fourier_mode, file path to save to
Next steps:
-Overwrite protection
-File open protection/error catching'''
import numpy as np
import matplotlib.pyplot as plt


def get_largest_fourier_modes(data):
    """
    data must be formatted such that the last axis
    is the one over which to take the fft (default setting),
    i.e. data[0,:] = z-data at time t, data[:, 0] = time data
    at z = 0.
    """
    spectrum = np.fft.rfft(data) #default setting: fft over last axis.
    freqs = np.fft.rfftfreq(data.shape[-1])
    magnitude = np.abs(spectrum)
    phase = np.angle(spectrum)
    # Take largest two mode data
    # Note: 0th entry is zero frequency term, sum of signal
    if data.ndim != 1:
        largest_mode_mag = magnitude[:, 1]
        largest_mode_phase = phase[:, 1]
        second_mode_mag = magnitude[:, 2]
        second_mode_phase = phase[:, 2]
    else:
        largest_mode_mag = magnitude[1]
        largest_mode_phase = phase[1]
        second_mode_mag = magnitude[2]
        second_mode_phase = phase[2]
    # Normalize magnitudes
    N_zones = data.shape[1]
    largest_mode_mag *= 2/N_zones
    second_mode_mag *= 2/N_zones
    return np.array([largest_mode_mag, largest_mode_phase, second_mode_mag, second_mode_phase])


qs = ["01","02","03","04","05","06","07","08","09","10","11", "12", "13", "14", "15"]
qs = ["13"]
for j in range(len(qs)-1, -1, -1):
    datax = np.loadtxt("/home/hankla/analysis_sheardynamo/data_reduced/xyazte_mean_field/xyazte_bx_hgbLAIF_r64z4b4000znf_q"+qs[j]+".txt")
    outnamex = "/home/hankla/analysis_sheardynamo/data_reduced/xyate_ft12/xyate_ft12_bx_hgbLAIF_r64z4b4000znf_q"+qs[j]+".txt"
    datay = np.loadtxt("/home/hankla/analysis_sheardynamo/data_reduced/xyazte_mean_field/xyazte_by_hgbLAIF_r64z4b4000znf_q"+qs[j]+".txt")
    outnamey = "/home/hankla/analysis_sheardynamo/data_reduced/xyate_ft12/xyate_ft12_by_hgbLAIF_r64z4b4000znf_q"+qs[j]+".txt"
    ft_bx = get_largest_fourier_modes(datax)
    ft_by = get_largest_fourier_modes(datay)

    np.savetxt(outnamex, np.transpose(ft_bx), header="bx1_mag bx1_phase bx2_mag bx2_phase")
    np.savetxt(outnamey, np.transpose(ft_by), header="by1_mag by1_phase by2_mag by2_phase")
    print("Saved "+outnamey)


#qs = ["15"]
#
#for j in range(len(qs)-1, -1, -1):
    #datax = np.loadtxt("/u/ahankla/analysis_sheardynamo/data_reduced/xyazte_mean_field/xyazte_bx_hgbLAIF_r64z4b4000znf_q"+qs[j]+".txt")
    #outnamex = "/u/ahankla/analysis_sheardynamo/data_reduced/xyate_ft12/xyate_ft12_bx_hgbLAIF_r64z4b4000znf_q"+qs[j]+".txt"
    #datay = np.loadtxt("/u/ahankla/analysis_sheardynamo/data_reduced/xyazte_mean_field/xyazte_by_hgbLAIF_r64z4b4000znf_q"+qs[j]+".txt")
    #outnamey = "/u/ahankla/analysis_sheardynamo/data_reduced/xyate_ft12/xyate_ft12_by_hgbLAIF_r64z4b4000znf_q"+qs[j]+".txt"
    #ft_bx = get_largest_fourier_modes(datax)
    #ft_by = get_largest_fourier_modes(datay)
#
    #np.savetxt(outnamex, np.transpose(ft_bx), header="bx1_mag bx1_phase bx2_mag bx2_phase")
    #np.savetxt(outnamey, np.transpose(ft_by), header="by1_mag by1_phase by2_mag by2_phase")
    #print("Saved "+outnamey)
#
#time = 1000
#N_zones = datay.shape[1]
#z = np.arange(0, N_zones)
#plt.figure()
#plt.plot(datay[time,:], label="data")
#plt.plot(ft_by[0, time]*np.cos(2*np.pi/N_zones*z+ft_by[1, time])+ft_by[2, time]*np.cos(4*np.pi/N_zones*z+ft_by[3, time]), ls="--", label="first two modes")
#plt.xlabel("z")
#plt.ylabel("By")
#plt.legend()
#plt.show()
