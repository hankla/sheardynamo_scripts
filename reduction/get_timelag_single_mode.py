"""
Use FT phase data to calculate time lag between mean radial and azimuthal field.
"""
import numpy as np
import matplotlib.pyplot as plt

qs = ["02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "14", "15"]
bx_modes = dict()
by_modes = dict()
ids = "hgbLAIF_r64z4b4000"
imgdir = "/home/hankla/analysis_sheardynamo/figures/ze_time_lag/"
iname = "ze_time_lag_"+ids


def construct_single_mode_b(b_mode_data, L_z):
    """
    Import b_mode_data as a function of z and t, formatted such that
    b_mode_data[:, 0] = b largest mode amplitude as a function of time,
    b_mode_data[0, :] = b largest and second-largest mode amp/phase at t=0.
    L_z is number of zones in the z-direction.
    """
    time_steps = by_mode_data.shape[0]
    z = np.arange(0, L_z)
    single_mode_b = []
    # Construct by out of mode data
    for t in range(time_steps):
        ft = b_mode_data[t, :]
        reconstructed = ft[0]*np.cos(2*np.pi/L_z*z+ft[1])
        single_mode_b.append(reconstructed)
    return np.array(single_mode_b)


def get_time_lag(bx, by, Tcycle):
    """
    Get the time lag between the mean field in the
    x and y directions by calculating the cross-correlation
    between the two time series at the same height z and
    taking the lag with the most negative correlation,
    as per SSH16 Sec. 4
    INPUTS:
    bx: mean field in the x direction,
    dependent on z and t, formatted such that
    bx[0] = bx(t=0,z), a list. bx[1] = bx(t = 1, z).
    bx[0][5] = bx(t=0, z=5)
    by: mean field in the y direction,
    dependent on z and t, formatted the
    same as bx.
    NOTE: Constrain time_lag to be between 0 and Tcycle.
    To get Tcycle, need to have run fig_psd_... and
    created cycle_period.txt output.
    NOTE: assume that this cycle_period.txt file has the
    same number of entries and is in the correct order!!
    OUTPUTS:
    Array of timelags at each height
    TO DO:
    Read cycle_period.txt first/second columns and match
    """
    # time_steps = len(bx)
    N_zones = len(bx[0])

    bx = np.array(bx)
    by = np.array(by)
    time_lag = []

    for z in np.arange(N_zones):
        # Get full correlation data
        c = np.correlate(bx[:, z], by[:, z], mode=2)
        Nx = bx[:, z].size
        # Cut window to be no greater than 1.5Tcycle
        # otherwise largest will be obv. the entire time
        maxlag = int(1.0*Tcycle) # cycles are in code time
        c = c[Nx - 1 - maxlag:Nx + maxlag]
        # Find index of most negative correlation
        # (pi out of phase)
        maxpos_ind = (-c).argmax()
        # Create array of lags
        lags = np.arange(-maxlag, maxlag + 1, 1)
        # Get lag of maximum correlation
        maxpos_lag = lags[maxpos_ind]
        time_lag.append(maxpos_lag)
    return np.array(time_lag)


cycle_data =np.loadtxt("/home/hankla/analysis_sheardynamo/data_reduced/cycle_period/cycle_period_best.txt")
cycle_period = cycle_data[:, 1]
mean_lags_rel = np.array([]); qvals = np.array([])

for j in range(len(qs)-1, -1, -1):
    fname_bx = "/home/hankla/analysis_sheardynamo/data_reduced/xyate_ft12/xyate_ft12_bx_hgbLAIF_r64z4b4000znf_q"+qs[j]+".txt"
    fname_by = "/home/hankla/analysis_sheardynamo/data_reduced/xyate_ft12/xyate_ft12_by_hgbLAIF_r64z4b4000znf_q"+qs[j]+".txt"

    bx_mode_data = np.loadtxt(fname_bx)
    by_mode_data = np.loadtxt(fname_by)

    single_mode_bx = construct_single_mode_b(bx_mode_data, 256)
    single_mode_by = construct_single_mode_b(by_mode_data, 256)

    Tcyc_orb = cycle_period[-j-1]/(2*np.pi)
    print(Tcyc_orb)
    # Correlate z-slices to find the time at which we get most lag

    time_lag = get_time_lag(single_mode_bx, single_mode_by, cycle_period[-j-1])
    # Convert to Tcyc and to orbits
    rel_lag = time_lag/cycle_period[-j-1] # in Tcycles
    orb_lag = time_lag/(2*np.pi) # in orbits

    qval = int(qs[j])/10.; qvals = np.append(qvals, qval)
    z_range = np.linspace(-2., 2., time_lag.size)

    # Get means
    mean_lag_orb = np.mean(orb_lag)
    mean_lag_rel = np.mean(rel_lag); mean_lags_rel = np.append(mean_lags_rel, mean_lag_rel)
    print(mean_lag_orb)

    # Plot!
    plt.figure()
#    plt.plot(z_range, rel_lag, label="largest vertical modes")
    plt.plot(z_range, -orb_lag, label="largest vertical modes")
    plt.xlabel("z [H]")
    plt.ylabel("lag (orbits)")
#    plt.ylabel(r"lag/$T_{cycle}$")
    plt.title("hgbLAIF_r64z4b4000znf_q{}".format(qs[j]))
    plt.axhline(-mean_lag_orb, c="black", linestyle='--', label="mean: {:2.1f} orbits".format(-mean_lag_orb))
    plt.legend(loc=1)
    plt.tight_layout()
    plt.savefig(imgdir+iname+"_q{}".format(qs[j])+".pdf")
    #plt.show()
    plt.close()

print(cycle_period)
print(mean_lags_rel)
plt.figure()
phase = 1 - 2./mean_lags_rel
plt.plot(qvals, phase, "bo")
plt.xlabel(r"shear $q$")
plt.ylabel(r"$\phi/\pi$")


imgdir = "/home/hankla/analysis_sheardynamo/figures/vataqe_time_lag/"
iname = "vataqe_time_lag_"+ids+".pdf"
plt.savefig(imgdir+iname)

plt.show()










