import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

def get_r2(func, xdata, ydata, params):
    resids = ydata - func(xdata, *params)
    ss_res = np.sum(resids**2)
    ss_tot = np.sum((ydata - np.mean(ydata))**2)
    r_squared = 1. - (ss_res/ss_tot)
    return r_squared


def moving_average(a, n=3):
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n


def construct_two_mode_by(by_mode_data, L_z):
    """
    Import by_mode_data as a function of z and t, formatted such that
    by_mode_data[:, 0] = by largest mode amplitude as a function of time,
    by_mode_data[0, :] = by largest and second-largest mode amp/phase at t=0.
    L_z is number of zones in the z-direction.
    """
    time_steps = by_mode_data.shape[0]
    z = np.arange(0, L_z)
    by = []
    # Construct by out of mode data
    for t in range(time_steps):
        ft = by_mode_data[t, :]
        reconstructed = ft[0]*np.cos(2*np.pi/L_z*z+ft[1])+ft[2]*np.cos(4*np.pi/L_z*z+ft[3])
        by.append(reconstructed)
    return np.array(by)


def fit_function(t, amp, tcycle, phase):
    return amp*np.sin(2*np.pi*t/tcycle + phase)


def fit_function2(t, a, omega, tauc):
    return ((1-a)+a*np.cos(2*np.pi*omega*t))*np.exp(-t/tauc)


def fit_cycle_period(by, fit_function_, tcyc_guess=200.):
    """
    Fit the PSD of the largest vertical mode to extract cycle period.
    NOTE: There are major problems with getting stuck in a local minimum.
    Trying to fix this via initial guess...200 seems for some reason a
    good choice for q >= 1.4
    """
    window = 10
    tstart = 600
    time_steps = by.shape[0]
    t = np.arange(tstart, time_steps)
    # Smooth data a bit (doesn't actually help...)
    byslice = moving_average(by, window)
    byslice = by[tstart:]
    print(byslice.shape)
    bymax = np.max(np.abs(byslice))
    bounds = ([0., 0., 0.], [bymax, time_steps, 2*np.pi])
    p0 = [bymax, tcyc_guess, 0.]
    params, pcov = curve_fit(fit_function_, t, byslice, p0=p0, bounds=bounds)
    r2 = get_r2(fit_function_, t, byslice, params)

    return params, r2


imgdir = "/home/hankla/analysis_sheardynamo/figures/"
idir = "xyate_cycle_period/"
processed = False
#qs = ["02", "03", "04", "05", "06", "07","08","09","10","11", "12", "14", "15"]
qs = ["13"]
#full_params = []
#for j in range(len(qs)-1, -1, -1):
    #fname = "/home/hankla/analysis_sheardynamo/data_reduced/xyate_ft12/xyate_ft12_by_"
    #if processed:
        #fname += "processed_"
    #ts = "hgbLAIF_r64z4b4000znf_q" + qs[j]
    #fname += (ts+".txt")
    #by_ft = np.loadtxt(fname)
    #print(by_ft.shape)
    #by_psd1 = by_ft[:,0]
    #by_psd1 = moving_average(by_psd1, 25)
    #by_psd1 = by_psd1[500:]
    #by_psd1 = by_psd1 - np.mean(by_psd1)
    #by_psd1 = by_psd1/np.max(np.abs(by_psd1))
#
    ## Now Fourier Transform the data
    #spectrum = np.fft.rfft(by_psd1)
    #magnitude = np.abs(spectrum)
    #magnitude = magnitude * 2./by_psd1.size
    #freqs = np.fft.rfftfreq(by_psd1.size)
    #phases = np.angle(spectrum)
    ##magnitude = moving_average(magnitude, 5)
#
    #inds = np.argsort(magnitude)
    #main_freqs = np.array([freqs[inds[-1]], freqs[inds[-2]]])
    #main_mags = np.array([magnitude[inds[-1]], magnitude[inds[-2]]])
    #main_phases = np.array([phases[inds[-1]], phases[inds[-2]]])
##
    ###plt.figure()
    ###plt.plot(magnitude)
###
    #plt.figure()
    #t = np.arange(by_psd1.size)
    #reconstructed = main_mags[0]*np.cos(2*np.pi*main_freqs[0]*t+main_phases[0])#+main_mags[1]*np.cos(2*np.pi*main_freqs[1]*t+main_phases[1])
    #reconstructed_second = main_mags[1]*np.cos(2*np.pi*main_freqs[1]*t+main_phases[1])#+main_mags[1]*np.cos(2*np.pi*main_freqs[1]*t+main_phases[1])
    #tc = (t + 500)/(2*np.pi)
    #plt.plot(tc, by_psd1, label="data")    ##plt.plot(tc, reconstructed,ls=":", label="largest FT, Tcyc={:4.0f}".format(1./main_freqs[0]), color="orange")
    ##plt.plot(tc, reconstructed_second, ls="--", label="second-largest FT, Tcyc={:4.0f}".format(1./main_freqs[1]), color="orange")
##
    #params_largest, r2_largest = fit_cycle_period(by_psd1, fit_function, 1./main_freqs[0])
    #params_slargest, r2_slargest = fit_cycle_period(by_psd1, fit_function, 1./main_freqs[1])
    ##plt.plot(tc, fit_function(t, *params_largest), label="fit: Tcyc={:4.0f}, r2: {:1.3}".format(params_largest[1], r2_largest), ls=":", color="green")
    ##plt.plot(tc, fit_function(t, *params_slargest), label="fit: Tcyc={:4.0f}, r2: {:1.3}".format(params_slargest[1], r2_slargest), ls="--", color="green")
#
    #fitted_amps = np.array([params_largest[0], params_slargest[0]])
    #fitted_Tcycs = np.array([params_largest[1], params_slargest[1]])
    #fitted_phase = np.array([params_largest[2], params_slargest[2]])
    #fitted_r2 = np.array([r2_largest, r2_slargest])
    #bestTcyc_ind = np.argmin([np.min(np.abs(fitted_Tcycs[0]-1./main_freqs[0])), np.min(np.abs(fitted_Tcycs[1]-1./main_freqs[1]))])
    #best_amp = fitted_amps[bestTcyc_ind]
    #bestTcyc = fitted_Tcycs[bestTcyc_ind]
    #best_phase = fitted_phase[bestTcyc_ind]
    #best_r2 = fitted_r2[bestTcyc_ind]
    #plt.plot(tc, best_amp*np.sin(2*np.pi*t/bestTcyc+best_phase), label="bestTcyc")
#
    #plt.xlabel("Time (orbits)")
    #plt.ylabel("Azimuthal Mean B(1) Amplitude")
    #plt.legend()
    #plt.title("hgbLAIF_r64z4b4000znf_q"+qs[j])
    #istr = "fig_xyate_cycle_period_best_"+ts+".pdf"
    ## istr = "fig_xyate_cycle_period_"+ts+".pdf"
    ##plt.savefig(imgdir+idir+istr)
    ### plt.show()
##
    #qval = int(qs[j])/10.
    #fit_params = np.array([qval, bestTcyc, best_phase, best_r2])
    #full_params.append(fit_params)
##
##full_params = np.array(full_params)
##print(full_params[0, :]) # qval, bestTcyc, etc for first qval = 1.5
##print(full_params[:, 0]) # All qvals
fname_params = "/home/hankla/analysis_sheardynamo/data_reduced/cycle_period/cycle_period_best.txt"
##np.savetxt(fname_params, full_params, header="qval Tcyc phase r2")
#
# ----------------------------------
#  Fit Cycle Period over Shear
# -----------------------------------
def power_law(q, amp, power):
    return amp*q**power


def power_law2(q, amp):
    return amp/q


def linear(q, slope, offset):
    return slope*q+offset


full_params = np.loadtxt(fname_params)
q_range = full_params[:, 0]
periods = full_params[:, 1]/(2*np.pi) # in orbits

params, pcov = curve_fit(power_law, q_range, periods)
amp, pcov2 = curve_fit(power_law2, q_range, periods)
linparams, pcovlin = curve_fit(linear, q_range, periods)

r2_power = get_r2(power_law, q_range, periods, params)
r2_inv = get_r2(power_law2, q_range, periods, amp)
r2_lin = get_r2(linear, q_range, periods, linparams)

print(params)
print(linparams)
q_full = np.arange(np.min(q_range),np.max(q_range)+0.1, 0.1)
plt.figure()
plt.plot(q_range, periods, "bo", label="measured")
plt.plot(q_full, power_law2(q_full, amp), "s", markerfacecolor='none', color="tab:orange", label=r"$\alpha\Omega$ prediction, $r^2$: {:.2}".format(r2_inv))
plt.plot(q_full, power_law(q_full, *params), ls="--", color="black", label=r"Power law fit: index = {:1.2f}, $r^2$: {:.2f}".format(params[1], r2_power))
plt.plot(q_full, linear(q_full, *linparams), ls=":", color="tab:green", label=r"Linear fit, $r^2$: {:.2}".format(r2_lin))
plt.legend()
plt.xlabel(r"Shearing parameter $q$")
plt.ylabel("Cycle Period (orbits)")
#plt.gca().set_yscale("log")
plt.title("hgbLAIF_r64z4b4000znf")
plt.tight_layout()
istr = "fig_q_cycle_period_hgbLAIF_r64z4b4000znf.pdf"
plt.savefig(imgdir+idir+istr)
plt.show()

#processed = False
#fit2 = False
#qs = ["15"] # ["02", "03", "04", "05", "06", "07","08","09","10","11", "12", "14", "15"]
#full_params = []
#for j in range(len(qs)-1, -1, -1):
    #fname = "/home/hankla/analysis_sheardynamo/data_reduced/xyate_ft12/xyate_ft12_by_"
    #if processed:
        #fname += "processed_"
    #fname += "hgbLAIF_r64z4b4000znf_q" + qs[j] + ".txt"
    #by_ft = np.loadtxt(fname)
    #by_psd1 = by_ft[:, 0]
    #if fit2:
        #params, r2 = fit_cycle_period(by_psd1, fit_function2, True)
    #else:
        #params, r2 = fit_cycle_period(by_psd1, fit_function)
    #fit_params = np.append(params, r2)
    #qval = int(qs[j])/10.
    #fit_params = np.insert(fit_params, 0, qval)
    #full_params.append(fit_params)
#
#full_params = np.array(full_params)
#print(full_params)
#print(full_params.shape)
#plt.figure()
#time_steps = by_psd1.shape[0]
#t = np.arange(0, time_steps)
#plt.plot(t, by_psd1, label="largest mode amplitude")
#if fit2:
    #plt.plot(t, fit_function2(t, *params), label="fit: ((1-{:2.2f})+{:2.2f}cos(2pi*t*{:3.2f})exp(-t/{:2.2}), r2: {:1.4f}".format(params[0], params[0], params[1], params[2], r2))
#else:
    #plt.plot(t, fit_function(t, *params), label="fit: {:2.2}sin(2*pi*t/{:3.2}+{:2.2})+{:3.2}, r2: {:1.3}".format(*params, r2))
#plt.xlabel("Time")
#plt.ylabel("By")
#plt.legend()
#
#by_ft2 = np.loadtxt("/home/hankla/analysis_sheardynamo/data_reduced/xyate_ft12/xyate_ft12_by_hgbLAIF_r64z4b4000znf_q15.txt")
#by_psd2 = by_ft2[:, 0]
#plt.figure()
#plt.plot(by_psd2)
#plt.plot(by_psd1)
#plt.show()

#np.savetxt("/u/ahankla/analysis_sheardynamo/data_reduced/cycle_period/cycle_period_processed_ft.txt", full_params, header="qs amp cycle phase offset r2")

#full_params = np.loadtxt("/u/ahankla/analysis_sheardynamo/data_reduced/cycle_period/cycle_period_psd.txt")
#
#plt.figure()
#plt.plot(full_params[:, 0], full_params[:, 2], "bo")
#plt.xlabel("shear q")
#plt.ylabel("Tcycle")
#plt.show()
#bx_ft = np.loadtxt("/u/ahankla/analysis_sheardynamo/data_reduced/xyate_ft12/xyate_ft12_bx_hgbLAIF_r64z4b4000znf_q14.txt")
#time_steps = by_psd1.shape[0]
#t = np.arange(0, time_steps)
#plt.plot(t, by_psd1, label="largest mode amplitude")
#plt.plot(t, fit_function(t, *params), label="fit: {:2.2f}sin(2pi*t/{:3.2f}+{:1.2f})+{:1.3f}, r2: {:1.4f}".format(*params, r2))
#plt.xlabel("Time")
#plt.ylabel("By")
#plt.legend()

