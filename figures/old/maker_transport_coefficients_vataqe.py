"""
Load in trials and run projection method.
"""
import numpy as np
import projection_method as pm
import matplotlib.pyplot as plt

qs = ["02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "14", "15"]
saved = True
C = dict()

for j in range(len(qs)-1, -1, -1):
    fname_c = "/home/hankla/analysis_sheardynamo/data_reduced/vate_transport_coefficients/vate_transport_coefficients_hgbLAIF_r64z4b4000znf_q"+qs[j]+".txt"

    if saved:
        Cq = np.loadtxt(fname_c)
        C[qs[j]] = Cq
    else:
        emfx = np.loadtxt("/u/ahankla/analysis_sheardynamo/data_reduced/xyazte_emf/xyazte_emfx_hgbLAIF_r64z4b4000znf_q"+qs[j]+".txt")
        emfy = np.loadtxt("/u/ahankla/analysis_sheardynamo/data_reduced/xyazte_emf/xyazte_emfy_hgbLAIF_r64z4b4000znf_q"+qs[j]+".txt")
        mean_bx = np.loadtxt("/u/ahankla/analysis_sheardynamo/data_reduced/xyazte_mean_field/xyazte_bx_hgbLAIF_r64z4b4000znf_q"+qs[j]+".txt")
        mean_by = np.loadtxt("/u/ahankla/analysis_sheardynamo/data_reduced/xyazte_mean_field/xyazte_by_hgbLAIF_r64z4b4000znf_q"+qs[j]+".txt")
        # Get coefficients!
        C = pm.get_all_transport_coefficients(emfx, emfy, mean_bx, mean_by, True)
        np.savetxt(fname_c, np.transpose(C), header="alpha_yy1 alpha_yy2 alpha_xy eta_xx1 eta_xx2 eta_yx")
        print("Saved " + fname_c)



## FIGURES
#colors=["tab:blue", "tab:orange", "tab:green", "tab:red", "tab:purple", "tab:pink"]
#c = 0
#plt.figure()
#for key, Cq in C.items():
    #plt.plot(Cq[0, :], label="alpha_yy 1", color=colors[c], ls="-")
    #plt.plot(Cq[1, :], label="alpha_yy 2", color=colors[c], ls="-.")
    #plt.axhline(np.mean(Cq[0, :]), color=colors[c], linestyle='--')
    #plt.axhline(np.mean(Cq[1, :]), color=colors[c], linestyle='--')
    #plt.legend()
    #plt.xlabel("Time")
    #plt.ylabel("alpha_yy")
    #c += 1
#plt.show()

key_list = np.array(C.keys())
num_keys = key_list.size
tmean_vals = np.zeros((num_keys, 6+1))
tstd_vals = np.zeros((num_keys, 6+1))
win = 100
tstart = 500
for j in range(num_keys):
    if win > 1:
        for k in range(0, C[key_list[j]].shape[1]):
            data_smooth = pm.moving_average(C[key_list[j]][:, k], win)
            if 'data' in locals():
                data = np.vstack((data, data_smooth))
            else:
                data = data_smooth
        data = np.transpose(data)
    else:
        data = C[key_list[j]]
    mean_vals = np.mean(data[tstart:], axis=0)
    std_vals = np.std(data[tstart:], axis=0)
    del data
    qval = int(key_list[j])/10.
    tmean_vals[j, :] = np.insert(mean_vals, 0, qval)
    tstd_vals[j, :] = np.insert(std_vals, 0, qval)


ids = "hgbLAIF_r64z4b4000"
imgdir = "/home/hankla/analysis_sheardynamo/figures/vataqe_transport_coefficients/"
iname = "vataqe_"+ids+"_tstart{}".format(tstart)+"win{}".format(win)

plt.figure()
plt.errorbar(tmean_vals[:, 0], tmean_vals[:, 1], yerr=tstd_vals[:, 1], color="tab:blue", marker="o", label="method 1", ls="None")
plt.errorbar(tmean_vals[:, 0], tmean_vals[:, 2], yerr=tstd_vals[:, 2], color="tab:red", marker="s", label="method 2", ls="None")
plt.axhline(0., color="black", linestyle='--')
plt.xlabel(r"shear $q$")
plt.ylabel(r"$\alpha_{yy}$")
plt.legend()
plt.tight_layout()
plt.savefig(imgdir+iname+"_alphayy.pdf")

plt.figure()
plt.errorbar(tmean_vals[:, 0], tmean_vals[:, 3], yerr=tstd_vals[:, 3], color="tab:blue", marker="o", ls="None")
plt.axhline(0., color="black", linestyle='--')
plt.xlabel(r"shear $q$")
plt.ylabel(r"$\alpha_{xy}$")
plt.tight_layout()
plt.savefig(imgdir+iname+"_alphaxy.pdf")

plt.figure()
plt.errorbar(tmean_vals[:, 0], tmean_vals[:, 4], yerr=tstd_vals[:, 4], color="tab:blue", marker="o", label="method 1", ls="None")
plt.errorbar(tmean_vals[:, 0], tmean_vals[:, 5], yerr=tstd_vals[:, 5], color="tab:red", marker="s", label="method 2", ls="None")
plt.axhline(0., color="black", linestyle='--')
plt.xlabel(r"shear $q$")
plt.ylabel(r"$\eta_{xx}$")
plt.legend()
plt.tight_layout()
plt.savefig(imgdir+iname+"_etaxx.pdf")

plt.figure()
plt.errorbar(tmean_vals[:, 0], tmean_vals[:, 6], yerr=tstd_vals[:, 6], color="tab:blue", marker="o", ls="None")
plt.axhline(0., color="black", linestyle='--')
plt.xlabel(r"shear $q$")
plt.ylabel(r"$\eta_{yx}$")
plt.tight_layout()
plt.savefig(imgdir+iname+"_etayx.pdf")


plt.show()
