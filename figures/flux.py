"""
-------------------------------------------------------
    OVERVIEW: This script uses Visit to extract the magnetic
    flux through the top of the shearing box as a function of
    radius x, having averaged over y and time.
Final radial profile is optionally output to a txt file

    INPUTS: 
    -visitpath: path to visit modules. Shouldn't need to be changed 
    -datapath: path to database to be analyzed, which should be .vtks output by athena with primitive variables. Also needs to include ' database' at the end to function with OpenDatabase
    -filename: name of the file where the condensed data will be output (to be read by matlab)
    OUTPUTS: 
    -f: the file where the condensed data will be output. Will be named as filename. Rows are the by values at one time...there should be 64 columns, since the z-resolution is 64 zones. The number of rows is the time...usually starting at t=0 and progressing in intervals of 0.1 orbits (as specified in the athena input file). Time is not explicitly a column, neither are the z-coordinates. 
    Author: Lia  Hankla
        Modifications
    2018/05/31: Created from Visit script produced through command's record function (unnecessary fluff removed)
------------------------------------------------------
"""
outputfile = True; filename="/home/hankla/analysis_sheardynamo/data_reduced/yate_mflux/yate_mflux_zm05_hgbLAIF_r32z1b4000q15.txt"
visitpath="/usr/local/misc/visit/visit2_12_3.linux-x86_64/2.12.3/linux-x86_64/lib/site-packages"
import sys
sys.path.append(visitpath)
sys.path.append('/home/hankla/analysis_sheardynamo/scripts')
from visit import *

r = "r32"; rval = 32
z = "z1"
b = "b4000"
q = "q15"
vpath = "/home/hankla/data_work/trial_shear_data/hgbLAIF_r32z1b4000znf_q15_data/final_vtk_prim"
vtkname = "hgbLAIF_r32z1b4000znf_q15"

f = open(filename, 'a')
datapath="localhost:"+vpath+"/"+vtkname+".*.vtk database"
#Launch() #comment out when doing batch
print(datapath)
OpenDatabase(datapath, 0)
DefineScalarExpression("bz", "cell_centered_B[2]")
AddPlot("Pseudocolor", "bz", 1, 1)
AddOperator("Slice", 1)
SetActivePlots(0)
SliceAtts = SliceAttributes()
SliceAtts.originType = SliceAtts.Intercept
SliceAtts.originIntercept = -0.5 
SliceAtts.normal = (0, 0, 1)
SliceAtts.axisType = SliceAtts.ZAxis
SliceAtts.upAxis = (0, 1, 0)
SliceAtts.project2d = 1
SetOperatorOptions(SliceAtts, 1)
DrawPlots()
HideActivePlots()

AddPlot("Curve", "operators/DataBinning/1D/mesh", 1, 1)
DataBinningAtts = DataBinningAttributes()
DataBinningAtts.numDimensions = DataBinningAtts.One  # One, Two, Three
DataBinningAtts.dim1BinBasedOn = DataBinningAtts.X  # X, Y, Z, Variable
DataBinningAtts.dim1SpecifyRange = 0
DataBinningAtts.dim1NumBins = rval 

DataBinningAtts.outOfBoundsBehavior = DataBinningAtts.Clamp  # Clamp, Discard
DataBinningAtts.reductionOperator = DataBinningAtts.Average  # Average, Minimum, Maximum, StandardDeviation, Variance, Sum, Count, RMS, PDF
DataBinningAtts.varForReduction = "bz"
DataBinningAtts.emptyVal = 0
DataBinningAtts.outputType = DataBinningAtts.OutputOnBins  # OutputOnBins, OutputOnInputMesh
DataBinningAtts.removeEmptyValFromCurve = 0
SetOperatorOptions(DataBinningAtts, 1)
DrawPlots()
DrawPlots()

if outputfile:
    vals = GetPlotInformation()["Curve"]
    for i in range(len(vals)/2):
        idx = i*2+1
        f.write(str(vals[idx])+' ')
    tmax=TimeSliderGetNStates()
    f = open(filename,'a')
    for j in range(tmax):
        vals = GetPlotInformation()["Curve"]
        for i in range(len(vals)/2):
            idx=i*2+1
            f.write(str(vals[idx])+' ')
        TimeSliderNextState()
        f.write('\n')
    f.close()

#exit() #needed to run this template repeatedly. otherwise will just enter interactive mode.
