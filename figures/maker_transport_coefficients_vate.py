"""
Load in trials and run projection method.
"""
import numpy as np
import matplotlib.pyplot as plt


# Initialization
qs = ["02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "14", "15"]
qs = ["05", "15"]
plot_compare = True
plot_shear = False # DO THIS WITH FIG_CREATE!!!

# Data-path information
imgdir = "/home/hankla/analysis_sheardynamo/figures/"
fname_c = "/home/hankla/analysis_sheardynamo/data_reduced/vate_transport_coefficients/vate_transport_coefficients_"
ids = "hgbLAIF_r64z4r2b4000znf"
qr = len(qs)
q_range = np.zeros(qr)
mean_alphayy = np.zeros((qr, 2))
mean_alphaxy = np.zeros((qr, 2))
mean_etayx = np.zeros((qr, 2))
mean_etaxx = np.zeros((qr, 2))
time_choice = 1
if time_choice == 0:
    # Dynamo linear growth start/end
    orb1 = 10; orb2 = 50
    tl1 = int(orb1*2*np.pi)
    tl2 = int(orb2*2*np.pi)
    ts = "_t{}-{}_".format(orb1, orb2)
else:
    # Dynamo saturation start/end times
    orb1 = 100
    td1 = int(orb1*2*np.pi)
    ts = "_t{}-end_".format(orb1)

# Order of the files
label_dict_full = {0:r"$\alpha_{xx}$", 1:r"$\alpha_{xy}$", 2:r"$-\eta_{xy}$", 3:r"$\eta_{xx}$",
                4:r"$\alpha_{yx}$", 5:r"$\alpha_{yy}$", 6:r"$-\eta_{yy}$", 7:r"$\eta_{yx}$"}
label_dict_const = {0:r"$\alpha_{yy}=\alpha_{xx}$", 1:r"$\alpha_{xy}$", 2:"$\eta_{yx}$", 3:"$\eta_{xx}=\eta_{yy}$"}

# Load in data
for j in range(len(qs)-1, -1, -1):
    tstr = ids+"_q"+qs[j]
    fend = "_"+tstr+".txt"

    C_full = np.loadtxt(fname_c + "full" + fend)
    C_const = np.loadtxt(fname_c + "const" + fend)
    if plot_shear:
        q_range[j] =int(qs[j])/10.
        if time_choice == 0:
            mean_alphayy[j, :] = [np.mean(C_const[tl1:tl2, 0]), np.std(C_const[tl1:tl2, 0])]
            mean_alphaxy[j, :] = [np.mean(C_const[tl1:tl2, 1]), np.std(C_const[tl1:tl2, 1])]
            mean_etayx[j, :] = [np.mean(C_const[tl1:tl2, 2]), np.std(C_const[tl1:tl2, 2])]
            mean_etaxx[j, :] = [np.mean(C_const[tl1:tl2, 3]), np.std(C_const[tl1:tl2, 3])]
        else:
            mean_alphayy[j, :] = [np.mean(C_const[td1:, 0]), np.std(C_const[td1:, 0])]
            mean_alphaxy[j, :] = [np.mean(C_const[td1:, 1]), np.std(C_const[td1:, 1])]
            mean_etayx[j, :] = [np.mean(C_const[td1:, 2]), np.std(C_const[td1:, 2])]
            mean_etaxx[j, :] = [np.mean(C_const[td1:, 3]), np.std(C_const[td1:, 3])]


    if plot_compare:
        t_orb = np.arange(C_full.shape[0])/(2*np.pi) # in orbits
        # Plot comparisons of full vs constrained
        idir = "vate_transport_coefficients/"
        # Diagonal alpha elements
        plt.figure()
        plt.plot(t_orb, C_full[:, 0], label="Unconstrained "+label_dict_full[0], ls="-")
        plt.plot(t_orb, C_full[:, 5], label="Unconstrained "+label_dict_full[5], ls=":")
        plt.plot(t_orb, C_const[:, 0], label="Constrained "+label_dict_const[0], ls="--")
        plt.title(tstr)
        plt.axhline(0.0, c="black", ls="--")
        plt.xlabel("Time (orbits)")
        plt.ylabel("$[H\Omega]$")
        plt.legend()
        plt.tight_layout()
        plt.savefig(imgdir+idir+"vate_compare_diagonal_alpha_"+tstr+".pdf")

        # Off-diagonal alpha elements
        plt.figure()
        plt.plot(t_orb, C_full[:, 1], label="Unconstrained "+label_dict_full[1], ls="-")
        plt.plot(t_orb, C_full[:, 4], label="Unconstrained "+label_dict_full[4], ls=":")
        plt.plot(t_orb, C_const[:, 1], label="Constrained "+label_dict_const[1], ls="--")
        plt.axhline(0.0, c="black", ls="--")
        plt.title(tstr)
        plt.xlabel("Time (orbits)")
        plt.ylabel("$[H\Omega]$")
        plt.legend()
        plt.tight_layout()
        plt.savefig(imgdir+idir+"vate_compare_offdiagonal_alpha_"+tstr+".pdf")

        # Diagonal eta elements
        plt.figure()
        plt.plot(t_orb, C_full[:, 3], label="Unconstrained "+label_dict_full[3], ls="-")
        plt.plot(t_orb, C_full[:, 6], label="Unconstrained "+label_dict_full[6], ls=":")
        plt.plot(t_orb, C_const[:, 3], label="Constrained "+label_dict_const[3], ls="--")
        plt.axhline(0.0, c="black", ls="--")
        plt.title(tstr)
        plt.xlabel("Time (orbits)")
        plt.ylabel("$[H^2\Omega]$")
        plt.legend()
        plt.tight_layout()
        plt.savefig(imgdir+idir+"vate_compare_diagonal_eta_"+tstr+".pdf")

        # Off-diagonal eta elements
        plt.figure()
        plt.plot(t_orb, C_full[:, 2], label="Unconstrained "+label_dict_full[2], ls="-")
        plt.plot(t_orb, C_full[:, 7], label="Unconstrained "+label_dict_full[7], ls=":")
        plt.plot(t_orb, C_const[:, 2], label="Constrained "+label_dict_const[2], ls="--")
        plt.axhline(0.0, c="black", ls="--")
        plt.title(tstr)
        plt.xlabel("Time (orbits)")
        plt.ylabel("$[H^2\Omega]$")
        plt.legend()
        plt.tight_layout()
        plt.savefig(imgdir+idir+"vate_compare_offdiagonal_eta_"+tstr+".pdf")

        plt.show()
        #plt.close('all')



if plot_shear:
    # Plot constrained values as a function of shear
    tstr = "Averaged in Linear Growth: {}-{} orbits".format(orb1, orb2) if time_choice == 0 else "Averaged over Saturation Period: {} orbits on".format(orb1)
    idir = "vataqe_transport_coefficients/"

    plt.figure()
    plt.errorbar(q_range, mean_alphaxy[:, 0], yerr=mean_alphaxy[:, 1], ls="None", marker="s")
    plt.axhline(0.0, c="black", ls="--")
    plt.xlabel(r"Shearing parameter $q$")
    plt.ylabel(label_dict_const[0])
    plt.title(tstr)
    plt.tight_layout()
    #plt.savefig(imgdir+idir+"vataqe_alphaxy_const"+ts+ids+".pdf")

    plt.figure()
    plt.errorbar(q_range, mean_alphayy[:, 0], yerr=mean_alphayy[:, 1], ls="None", marker="s")
    plt.axhline(0.0, c="black", ls="--")
    plt.xlabel(r"Shearing parameter $q$")
    plt.ylabel(label_dict_const[1])
    plt.title(tstr)
    plt.tight_layout()
    #plt.savefig(imgdir+idir+"vataqe_alphayy_const"+ts+ids+".pdf")

    plt.figure()
    plt.errorbar(q_range, mean_etaxx[:, 0], yerr=mean_etaxx[:, 1], ls="None", marker="s")
    plt.axhline(0.0, c="black", ls="--")
    plt.xlabel(r"Shearing parameter $q$")
    plt.ylabel(label_dict_const[3])
    plt.title(tstr)
    plt.tight_layout()
    #plt.savefig(imgdir+idir+"vataqe_etaxx_const"+ts+ids+".pdf")

    plt.figure()
    plt.errorbar(q_range, -mean_etayx[:, 0], yerr=mean_etayx[:, 1], ls="None", marker="s")
    plt.axhline(0.0, c="black", ls="--")
    plt.xlabel(r"Shearing parameter $q$")
    plt.ylabel("-"+label_dict_const[2])
    plt.title(tstr)
    plt.tight_layout()
    #plt.savefig(imgdir+idir+"vataqe_etayx_const"+ts+ids+".pdf")

plt.show()
