import sys
sys.path.append('/home/hankla/analysis_sheardynamo/scripts')
import singlerun as sr
import fig_create as fc
from astropy.table import Table
from astropy.io import ascii
import os.path
import matplotlib.pyplot as plt
from time import time
import numpy as np
from scipy.optimize import curve_fit

#--------------------- INPUTS -------------------
trialstr = ["r64z4r2b4000q05", "r64z4r2b4000q15","r32z1b4000q05","r32z1b4000q09","r32z1b4000q12","r32z1b4000q15","r64z4b4000q111","r64z4b4000q113", "r64z4b4000q115", "r64z4b4000q118", "r64z1b4000q03","r64z1b4000q04","r64z1b4000q05","r64z1b4000q06","r64z1b4000q07","r64z1b4000q09","r64z1b4000q10","r64z1b4000q11","r64z1b4000q12","r64z1b4000q13","r64z1b4000q14","r64z1b4000q15", "r64z4b4000q02","r64z4b4000q03","r64z4b4000q04","r64z4b4000q05","r64z4b4000q06","r64z4b4000q07","r64z4b4000q08","r64z4b4000q09","r64z4b4000q10","r64z4b4000q11","r64z4b4000q12", "r64z4b4000q13","r64z4b4000q14","r64z4b4000q15","r128z1b4000q05","r128z1b4000q09","r128z1b4000q12","r128z1b4000q15"] #ALL
#trialstr = ["r32z1b4000q05","r32z1b4000q09","r32z1b4000q12","r32z1b4000q15","r64z1b4000q03","r64z1b4000q04","r64z1b4000q05","r64z1b4000q06","r64z1b4000q07","r64z1b4000q09","r64z1b4000q10","r64z1b4000q11","r64z1b4000q12","r64z1b4000q13","r64z1b4000q14","r64z1b4000q15","r128z1b4000q05","r128z1b4000q09","r128z1b4000q12","r128z1b4000q15","r64z4b4000q02","r64z4b4000q03","r64z4b4000q04","r64z4b4000q05","r64z4b4000q06","r64z4b4000q07","r64z4b4000q08","r64z4b4000q09","r64z4b4000q10","r64z4b4000q11","r64z4b4000q12","r64z4b4000q13","r64z4b4000q14","r64z4b4000q15"] #ALL
#trialstr = ["r64z1b4000q07","r64z1b4000q09","r64z1b4000q10","r64z1b4000q11","r64z1b4000q12","r64z1b4000q13", "r64z1b4000q14", "r64z4b4000q02","r64z4b4000q03","r64z4b4000q04","r64z4b4000q05","r64z4b4000q06","r64z4b4000q07","r64z4b4000q08","r64z4b4000q09","r64z4b4000q10","r64z4b4000q11","r64z4b4000q111", "r64z4b4000q113","r64z4b4000q115", "r64z4b4000q118","r64z4b4000q12","r64z4b4000q14","r64z4b4000q15"] # only r64z4
#trialstr = ["r32z1b4000q05","r32z1b4000q09","r32z1b4000q12","r32z1b4000q15","r64z1b4000q03","r64z1b4000q04","r64z1b4000q05","r64z1b4000q06","r64z1b4000q07","r64z1b4000q09","r64z1b4000q10","r64z1b4000q11","r64z1b4000q12","r64z1b4000q14","r64z1b4000q15","r128z1b4000q05","r128z1b4000q09","r128z1b4000q12","r128z1b4000q15"] #only short boxes
#trialstr = ["r64z1b4000q03","r64z1b4000q04","r64z1b4000q05","r64z1b4000q06","r64z1b4000q07","r64z1b4000q09","r64z1b4000q10","r64z1b4000q11","r64z1b4000q12","r64z1b4000q14","r64z1b4000q15"] #only r64z1
#trialstr = ["r64z4b4000q05","r64z4b4000q09","r64z4b4000q12","r64z4b4000q15"]
#trialstr = ["r64z4b4000q08","r64z4b4000q09", "r64z4b4000q11","r64z4b4000q12", "r64z4b4000q13", "r64z4b4000q14", "r64z4b4000q15"] #make sure to change butterfly cstr and dstr!!
#trialstr = ["r64z1b4000q15","r64z1b4000q14"]
#trialstr = ["r32z1b4000q05","r32z1b4000q09","r32z1b4000q12","r32z1b4000q15"] #all r32
#trialstr = ["r128z1b4000q05","r128z1b4000q09","r128z1b4000q12","r128z1b4000q15"] #all r128
#trialstr = ["r64z4b4000q02","r64z4b4000q03","r64z4b4000q04","r64z4b4000q05","r64z4b4000q06","r64z4b4000q07"]
#trialstr = ["r64z4b4000q08"]
#trialstr = ["r64z4b4000q09"]
#trialstr = ["r64z4b4000q09"] #,"r64z4b4000q12","r64z4b4000q14", "r64z4b4000q15"]
#trialstr = ["r64z1b4000q15", "r64z4b4000q11","r64z4b4000q12","r64z4b4000q14","r64z4b4000q15"]
#trialstr = ["r64z4b4000q08"] # only r64z4


ids = "hgbLAIF"
imgdir = "/home/hankla/analysis_sheardynamo/figures/"
#----------- INITIALIZATION -------------------
#for ts in trialstr:
    #dstr+=ts.replace(cstr,'')
f = 1
fname="runs.pickle"

# Either pickle...
runs = sr.initialize(trialstr,ids)
#pfile = open(fname,"wb")
#cp.dump(runs,pfile)
#pfile.close()
#...or load from pickle
#pfile = open(fname,"rb")
#runs = cp.load(pfile)

#---------- GENERATE FIGURES ----------------
# # Table of SSH value s
# t = fc.fig_vata_mtT_table(runs)
# print(t)
# tdir = "/home/hankla/analysis_sheardynamo/figures/"
# tstr = "tab_vata_mtT.txt"
# tf = open(tdir+tstr,"w")
# ascii.write(t,tf,Writer=ascii.Latex,latexdict={'preamble':r'\begin{adjustbox}{max width=\textwidth}','tablefoot':r'\end{adjustbox}','tabletype':'table*'},caption=r'Table of values for simulation runs. \label{tab:sshvals}')


# ----------------------------
#    Fastest-Growing Shear-current Mode
#    as a function of shear
# ---------------------------
#idir = "vataqe_fastest_sc_mode/"
#fc.fig_vataqe_fastest_mode(runs, f); f += 1
#istr = "fig_vataqe_" + key.replace("_","") + "_const_t100-end_" + ids
#if err:
    #istr += "_err"
#istr += ".pdf"
#fc.figsave(imgdir+idir,istr)
#plt.show()


# ----------------------------
#    Transport Coefficients
#    as a function of shear
# ---------------------------
#err = True
#key_list = {"alpha_yy": 0, "alpha_xy": 1, "eta_yx" : 2, "eta_xx" : 3}
#idir = "vataqe_transport_coefficients/"
#for key in key_list:
    #fc.fig_vataqe_transport_coefficients(runs, key, f, err); f += 1
    #istr = "fig_vataqe_" + key.replace("_","") + "_const_t100-end_" + ids
    #if err:
        #istr += "_err"
    #istr += ".pdf"
    #fc.figsave(imgdir+idir,istr)
    ##plt.show()
    ##plt.close()


# ---------------------------
#  Two-mode vs full Butterfly
# ---------------------------
#qs = ["02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "14", "15"]
#for q in qs:
    #run = sr.SingleRun(q, "64", "4", "4000", "hgbLAIF")
    #ts = run.idstr+"_r"+run.rval+"z"+run.zval+"b"+run.bval+"_q"+run.qval
    #fc.fig_xyazte_butterfly_mode_compare(run, f); f += 1
    #istr = "fig_xyazte_butterfly_mode_compare_"+ts+".pdf"
    #idir = "xyazte_butterfly_mode_compare/"
    #fc.figsave(imgdir+idir, istr)
#plt.show()

# ---------------------------
#  Log Butterfly
# ---------------------------
#qs = ["02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "14", "15"]
#for q in qs:
    #run = sr.SingleRun(q, "64", "4", "4000", "hgbLAIF")
    #ts = run.idstr+"_r"+run.rval+"z"+run.zval+"b"+run.bval+"_q"+run.qval
    #fc.fig_xyazte_butterfly_log(run, f); f += 1
    #istr = "fig_xyazte_butterfly_log_"+ts+".pdf"
    #idir = "xyazte_butterfly_log/"
    #fc.figsave(imgdir+idir, istr)
#plt.show()

# ---------------------------
#   Magnetic Field Time Lag
# ---------------------------
#"""
#Compare the time lag between
#the radial and azimuthal magnetic
#fields (largest vertical mode only).
#NOTE: Need to have run get_timelag.py
#while saving the largest fourier mode.
#"""
#bx1 = np.loadtxt("/home/hankla/analysis_sheardynamo/data_reduced/xyazte_ft1/")
#by1 = np.loadtxt("/home/hankla/analysis_sheardynamo/data_reduced/xyazte_ft1/")
#timelags = np.loadtxt("/home/hankla/analysis_sheardynamo/data_reduced/xyaze_timelag/xyaze_timelag_hgbLAIF_r64")




## ---------------------------
##    Dynamo Cycle Period
## ---------------------------
#T_cycles = np.array([])
#for ts in trialstr:
    #rkey = ts[ts.find("r"):ts.find("z")]
    #zkey = ts[ts.find('z'):ts.find('b')]
    #bkey = ts[ts.find('b'):ts.find('q')]
    #qkey = ts[ts.find('q'):]
    #run = runs[rkey][zkey][bkey][qkey]
    #h, T_cycles = fc.fig_xyate_psd_period(run, f, T_cycles); f += 1
    #print(T_cycles)
    #istr = "fig_xyate_psd_period_"+ts+".pdf"
    #idir = "xyate_psd_period/"
    ### do NOT figsave just in case period is different...
    ### if do change something, need to make sure timelag
    ### period is also up-to-date
    ## fc.figsave(imgdir+idir, istr)
    #plt.show()


## -------------------------------
##       Dynamo Cycle Period:
##      Print to File
## -------------------------------
#"""
#CAREFUL!!! Do not do this lightly...
#the periods are needed to accurately
#calculate the time lag, but the fitting
#function in fig_xyate_psd_period is not
#perfect so needs to be tweaked.
#DO NOT RUN unless absolutely certain...
#"""
#fnamesave = "/home/hankla/analysis_sheardynamo/data_reduced/cycle_period/cycle_period.txt"
#T_cycles = np.array([])
#for ts in trialstr:
    #rkey = ts[ts.find("r"):ts.find("z")]
    #zkey = ts[ts.find('z'):ts.find('b')]
    #bkey = ts[ts.find('b'):ts.find('q')]
    #qkey = ts[ts.find('q'):]
    #run = runs[rkey][zkey][bkey][qkey]
    #h, T_cycles = fc.fig_xyate_psd_period(run, f, T_cycles); f += 1
    #istr = "fig_xyate_psd_period_"+ts+".pdf"
    #idir = "xyate_psd_period/"
    #fc.figsave(imgdir+idir, istr)
    #plt.show()
#
#print(T_cycles)
#olddata = np.array(np.loadtxt(fnamesave) if (os.path.exists(fnamesave) and os.path.getsize(fnamesave) > 0) else [])
#if olddata.size != 0:
    #T_cycles = np.vstack((olddata, T_cycles))
#with open(fnamesave, 'wb') as file:
    #np.savetxt(file, T_cycles, fmt='%d %d %f %f', header="z q Tcycle rsquared")
##print(T_cycles[:,2]) #all the cycle periods
#

# ----------------------------
#    Energy spectrum
# ----------------------------

#fc.fig_vate_fft_hst(runs,"ME", f); f += 1
#plt.show()


## ---------------------------
##     Flux Radial Profile
## ---------------------------
#r = "r32"
#z = "z1"
#b = "b4000"
#q = "q15"
##vpath = "/home/hankla/data_work/trial_shear_data/hgbLAIF_r32z1b4000znf_q15/final_vtk_prim"
##vtkname = "hgbLAIF_r32z1b4000znf_q15"
##runs[r][z][b][q].set_mpath(mpath)
###
##fc.fig_yata_mflux(runs[r][z][b][q],f); f+= 1
##istr = "fig_yata_mflux.pdf"
##idir = "yata_mflux/"
## fc.figsave(imgdir+idir, istr)
##


#f, (ax1, ax2) = plt.subplots(2,1)
#ax1.plot(v1,label="hgbLAIF_r32z1b4000q15")
#ax2.plot(v2,label="hgbLAIF_r32z1b4000q15")
#ax1.axhline(np.mean(v1), xmin = 0.0, xmax = 1.0,color='k',linestyle=':',label="Mean value: {:.3E}".format(np.mean(v1)))
#ax2.axhline(np.mean(v2), xmin = 0.0, xmax = 1.0,color='k',linestyle=':',label="Mean value: {:.3E}".format(np.mean(v2)))
#ax1.set_title("Time: {} Orbits".format(t1/10))
#ax2.set_title("Time: {} Orbits".format(t2/10))
#ax1.set_ylabel("y-averaged Flux")
#ax2.set_ylabel("y-averaged Flux")
#ax2.set_xlabel("x (zones)")
#ax1.legend()
#ax2.legend()
#plt.tight_layout()
#istr = "fig_yata_mflux_m05_t{}t{}_hgbLAIF_r32z1b4000q15.pdf".format(t1,t2)
#idir = "yata_mflux/"
#fc.figsave(imgdir+idir, istr)
#
#plt.show()

## -------------------------
## Butterfly Diagram
## -------------------------
#toscale = False
#cstr = "r64z4b4000"; dstr="q06q09q12q15" #constant and different strings
#cstr = "r64b4000q15"; dstr="z1z4"
#h=fc.fig_xyte_by(runs,f,toscale); f+=1
#istr = "fig_xyte_by_"+ids+"_"+cstr+"_"+dstr
#if toscale:
    #istr+= "_scaled"
    #istr+=".pdf"
    #idir = "xyte_by/"
    #fc.figsave(imgdir+idir,istr)
    #plt.show()

# -----------------------------------------
#       Stress-Energy Ratios
# ------------------------------------------
#fc.fig_vata_memx_nb7(runs,f); f += 1
#istr = "fig_vata_memx_nb7.pdf"
#idir = "vata_memx_nb7/"
#fc.figsave(imgdir+idir,istr)


# -------------------------------------
# An SSH value as a function of shear/resolution/box size
#idir = "vata_mtT/"
#err = False
#sshkeys = ["MEtot","MEmean","MEymean","MEmtr","MXmTr","MEymtr","mamean","maturb"]
#for kval in sshkeys:
    #fc.fig_vata_mtT_plot(runs,kval,f, err); f+=1
    #istr = "fig_vata_mtT_"+kval
    #if err:
        #istr += "_err"
    #istr += ".pdf"
    #fc.figsave(imgdir+idir,istr)
    ##plt.show()

#hstkeys = ["Rx","MEx","MEy","MEz","Bx","By","Bz","Mx","Tx"]
#for kval in hstkeys:
    #fc.fig_vata_hst(runs,kval,f); f+=1
    #istr = "fig_vata_hst_"+kval+".pdf"
    #idir = "vata_hst/"
    #fc.figsave(imgdir+idir,istr)
    #plt.show()
    #plt.close()

#boolvals = [[True, True],[True, False],[False, True],[False, False]]
#for bv in boolvals:
    #istr = "fig_vata_mxrx_ssh7"
    #islog = bv[0]
    #dofit = bv[1]
    #if islog:
        #istr += "_log"
    #if dofit:
        #istr += "_fit"
    #istr += "_z1r128.pdf"
    #idir = "vata_mxrx_ssh7/"
    #fc.fig_vata_mxrx_ssh7(runs,f,islog,dofit); f+=1
    #fc.figsave(imgdir+idir,istr)
    #plt.close()
#
#for bv in boolvals:
    #istr = "fig_vata_tx_gp1"
    #islog = bv[0]
    #dofit = bv[1]
    #if islog:
        #istr += "_log"
    #if dofit:
        #istr += "_fit"
    #istr += "_z1r128.pdf"
    #idir = "vata_tx_gp1/"
    #fc.fig_vata_tx_gp1(runs,f,islog,dofit); f+=1
    #fc.figsave(imgdir+idir, istr)
    #plt.close()
## plt.show()
#
# -------------------------------------
#         Total Stress Fit Comparisons
# -----------------------------------
#idir = "vata_tx_gp1/"
#istr = "fig_vata_tx_gp1_fitcompare_r128z1.pdf"
#fc.fig_vata_tx_fitcompare(runs, f); f += 1
#plt.close()

# --------------------------------
#    Dynamo Growth Rate
#    via <BxBy> Growth
# -------------------------------
#idir = "vate_growth_rate/"
#islog = False
#
#rk = "r64"; zk = "z4"; bk = "b4000"
#runset = dict(); runset[rk]=dict(); runset[rk][zk]=dict(); runset[rk][zk][bk] = dict();
#runs[rk][zk][bk]["q14"].tl1 = 10; runs[rk][zk][bk]["q14"].tl2 = 70
#runs[rk][zk][bk]["q13"].tl1 = 10; runs[rk][zk][bk]["q13"].tl2 = 90 #90
#runs[rk][zk][bk]["q12"].tl1 = 10; runs[rk][zk][bk]["q12"].tl2 = 90 #90
#runs[rk][zk][bk]["q118"].tl1 = 10; runs[rk][zk][bk]["q118"].tl2 = 150 #115
#runs[rk][zk][bk]["q115"].tl1 = 10; runs[rk][zk][bk]["q115"].tl2 = 100 #100
#runs[rk]["z4r2"][bk]["q15"].tl1 = 10; runs[rk]["z4r2"][bk]["q15"].tl2 = 73
#qstr= ""
#qs = ["q115", "q12", "q13", "q14", "q15"]
##qs = ["q12"]
#runset[rk]["z4r2"]=dict(); runset[rk]["z4r2"][bk] = dict()
#run = runs[rk]["z4r2"][bk]["q15"]
#runset[rk]["z4r2"][bk]["q15"] = run
#fc.fig_vate_mean_field_fit(run, f, islog); f += 1
#
#for qk in qs: #sorted(runs[rk][zk][bk].keys()):
    #runset[rk][zk][bk][qk] = runs[rk][zk][bk][qk]
    #fc.fig_vate_mean_field_fit(runs[rk][zk][bk][qk], f, islog); f += 1
    #istr = "fig_vate_meanbxby_growth_rate"
    #runstr = "_"+rk+zk+bk
    #istr += runstr
    #istr += qk
    #if islog:
        #istr += "_log"
    #istr += ".pdf"
    #fc.figsave(imgdir+idir, istr)
    #qstr += qk
##plt.show()
#plt.close('all')
#idir = "vataqe_growth_rate/"
#istr = "fig_vataqe_meanbxby_growth_rate"
#istr = istr + runstr + qstr
#istr += ".pdf"
#print(istr)
#qvals, growth_rates = fc.fig_vataqe_mean_field_fit(runset, f)[1:]; f += 1
#fc.figsave(imgdir+idir, istr)
#plt.show()

# --------------------------------
#    Dynamo Growth Rate
#     via Magnetic Energy
# -------------------------------
#idir = "vate_growth_rate/"
#islog = False
#
#rk = "r64"; zk = "z4"; bk = "b4000"
#runset = dict(); runset[rk]=dict(); runset[rk][zk]=dict(); runset[rk][zk][bk] = dict();
#tend = dict(); tend[rk] = dict(); tend[rk][zk] = dict(); tend[rk][zk][bk] = dict()
#tstart = dict(); tstart[rk] = dict(); tstart[rk][zk] = dict(); tstart[rk][zk][bk] = dict()
#tstart[rk][zk][bk]["q15"] = 10; tend[rk][zk][bk]["q15"] = 50
#tstart[rk][zk][bk]["q14"] = 10; tend[rk][zk][bk]["q14"] = 50
#tstart[rk][zk][bk]["q13"] = 10; tend[rk][zk][bk]["q13"] = 50
#tstart[rk][zk][bk]["q12"] = 35; tend[rk][zk][bk]["q12"] = 70
#tstart[rk][zk][bk]["q11"] = 20; tend[rk][zk][bk]["q11"] = 40
#tstart[rk][zk][bk]["q10"] = 30; tend[rk][zk][bk]["q10"] = 75 #just looks linear...no work
#tstart[rk][zk][bk]["q09"] = 10; tend[rk][zk][bk]["q09"] = 60
#tstart[rk][zk][bk]["q08"] = 15; tend[rk][zk][bk]["q08"] = 35
#runstr = "_"+rk+zk+bk+"_"
#istr = "fig_vate_ME_growth_rate"+runstr
#
#runset = runs
#for qk in sorted(runs[rk][zk][bk].keys()):
    #runset[rk][zk][bk][qk] = runs[rk][zk][bk][qk]
    #istr += qk
#if islog:
    #istr += "_log"
#istr += ".pdf"
#qvals, growth_rates = fc.fig_vate_ME_fit(runset, f, tstart, tend, islog)[1:]; f += 1
#plt.close('all')
#print(growth_rates)
##fc.figsave(imgdir+idir, istr)
#
#def lin(q, A, off):
    #return A*q+off
#
#def quad(q, A, off):
    #return A*q**2+off
#
#params, pcov = curve_fit(lin, qvals, growth_rates)
#params2, pcov2 = curve_fit(quad, qvals, growth_rates)
#
#plt.figure()
#plt.plot(qvals, 1./growth_rates, "bo")
#plt.plot(qvals, 1./lin(qvals, *params), c="black", ls="--", label=r"$\sim q$")
#plt.plot(qvals, 1./quad(qvals, *params2), c='black', ls=":", label=r"$\sim q^2$")
#plt.xlabel(r"shear $q$")
#plt.ylabel("Fitted Dynamo Growth Rate (orbits)")
#plt.title("hgbLAIF_r64z4b4000znf")
#plt.legend()
#plt.tight_layout()
#idir = "qe_growth_rate/"
#istr = "fig_qe_growth_rate_hgbLAIF_r64z4b4000znf.pdf"
#fc.figsave(imgdir+idir, istr)
#plt.show()

# --------------------------------
#        Plot Time Evolutions
# -------------------------------
#kval = "ME"
#idir = "vate_hst/"+kval+"/"
#islog = False
#
#rk = "r64"; zk = "z4"; bk = "b4000"
#runset = dict(); runset[rk]=dict(); runset[rk][zk]=dict(); runset[rk][zk][bk] = dict();
#runstr = "_"+rk+zk+bk+"_"
#istr = "fig_vate_hst"+runstr
#
##runset = runs
#qs = ["q111", "q113", "q115", "q118"]
#for qk in qs: #sorted(runs[rk][zk][bk].keys()):
    #runset[rk][zk][bk][qk] = runs[rk][zk][bk][qk]
    #istr += qk
#if islog:
    #istr += "_log"
#istr += "_"+kval+".pdf"
#print(istr)
#fc.fig_vate_hst(runset,kval,f,islog); f += 1
#fc.figsave(imgdir+idir, istr)
#plt.show()


## -----------------------------------
##      Time Evolution Comparisons
## -----------------------------------
#idir = "vate_hst/"
#fc.fig_compare_vate_hst_mtT(runs, f); f += 2
#istr = "fig_compare_vate_hst_mtT.pdf"
#fc.figsave(imgdir+idir, istr)
#
#fc.fig_compare_MxRx(runs["r64"]["z1"]["b4000"]["q15"], f); f += 1
#istr = "fig_compare_MxRx.pdf"
#fc.figsave(imgdir+idir, istr)
##plt.show()
