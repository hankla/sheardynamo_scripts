import matplotlib.pyplot as plt
import numpy as np

by_z4q15 = np.loadtxt("/home/hankla/analysis_sheardynamo/data_reduced/xyazte_mean_field/xyazte_by_hgbLAIF_r64z4b4000znf_q14.txt")
by_z4q05 = np.loadtxt("/home/hankla/analysis_sheardynamo/data_reduced/xyazte_mean_field/xyazte_by_hgbLAIF_r64z4b4000znf_q06.txt")
by_z1q15 = np.loadtxt("/home/hankla/analysis_sheardynamo/data_reduced/xyazte_mean_field/xyazte_by_hgbLAIF_r64z1b4000znf_q14.txt")

# Make into 4 scaleheights
by_z1q15 = np.hstack([by_z1q15, by_z1q15, by_z1q15, by_z1q15])

bydata = np.array([by_z4q15, by_z4q05, by_z1q15])
num_dsets = bydata.size
maxes = np.zeros(num_dsets); mins = np.zeros(num_dsets); times = np.zeros(num_dsets)
for set in range(num_dsets):
    maxes[set] = np.max(bydata[set])
    mins[set] = np.min(bydata[set])
    times[set] = bydata[set].shape[0]/(2*np.pi)


mmax = np.max(maxes)
by_z4q05 = by_z4q05
by_z4q15 = by_z4q15
by_z1q15 = by_z1q15

sorted_inds = np.argsort(times)
fig, axes = plt.subplots(num_dsets, 1, sharex=True)

vmin = np.min(mins)
vmax = np.max(maxes)


t4q15 = by_z4q15.shape[0]/(2*np.pi)
t1q15 = by_z1q15.shape[0]/(2*np.pi)
t4q05 = by_z4q05.shape[0]/(2*np.pi)

extent4q15 = [0, t4q15, -2., 2.]
extent1q15 = [0, t1q15, -2., 2.]
extent4q05 = [0, t4q05, -2., 2.]

im = axes[0].imshow(np.transpose(by_z4q15), extent=extent4q15, aspect="auto", vmin=vmin, vmax=vmax)
axes[0].set_ylabel("z [H]")
axes[0].set_title(r"$L_z=4$, $q=1.4$")
cax0 = fig.colorbar(im, ax=axes[0])
cax0.set_label(r"$\langle B_y\rangle$")
#axes[0].axhline(0., c="black", ls=":")
im1 = axes[1].imshow(np.transpose(by_z1q15)*10, extent=extent1q15, aspect="auto", vmin=vmin, vmax=vmax)
axes[1].set_ylabel("z [H]")
axes[1].set_title(r"$L_z=1$, $q=1.4$")
axes[1].axhline(-1., c="black", ls=":")
cax1 = plt.colorbar(im1, ax=axes[1])
cax1.set_label(r"$10\langle B_y\rangle$")
axes[2].imshow(np.transpose(by_z4q05)*10, extent=extent4q05, aspect="auto", vmin=vmin, vmax=vmax)
axes[2].set_ylabel("z [H]")
axes[2].set_title(r"$L_z=4$, $q=0.6$")
axes[2].set_xlabel("Time (orbits)")
#axes[2].axhline(0., c="black", ls=":")
plt.tight_layout()
plt.savefig("/home/hankla/analysis_sheardynamo/figures/xyazte_butterfly_mode_compare/fig_xyazte_butterfly_compare_qz.pdf")
plt.show()
