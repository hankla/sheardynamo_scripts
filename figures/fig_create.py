from astropy.table import Table
import singlerun as sr
import numpy as np
from decimal import Decimal
import matplotlib.pyplot as plt
from matplotlib import colors
import os
from scipy.optimize import curve_fit
import sys
'''SUMMARY:
INPUTS:
In ALL of the functions below, runs is a dictionary of SingleRun objects as output by
the singlerun.initialize function. f is the figure number. Note that setting e.g.
h = fig_xyte_by(runs,f) will give the relevant figure handle. Note that f is NOT
increased by the functions below.
'''

#----------------------------------------------------------------
def fig_vataqe_transport_coefficients(runs, key, f, err=False, islog=False, markers=None, colors=None):
    key_list = {"alpha_yy": 0, "alpha_xy": 1, "eta_yx" : 2, "eta_xx" : 3}
    label_dict_const = {0:r"$\alpha_{yy}=\alpha_{xx}$ $[H\Omega]$", 1:r"$\alpha_{xy}$ $[H\Omega]$", 2:"$-\eta_{yx}$ $[H^2\Omega]$", 3:"$\eta_{xx}=\eta_{yy}=\eta_t$ $[H^2\Omega]$"}
    ind = key_list[key]
    cdict=dict(); c=0
    mdict=dict(); r=0
    if markers is None:
        markers = ["o","s","v"] #usually only have 3 resolutions
        #default colors values
    if colors is None:
        colors = ["green","blue","orange"] #usually have 2 box sizes

    h = plt.figure(f)
    ax = plt.gca()
    for rkey in sorted(runs.keys()):
        if rkey not in mdict:
            mdict[rkey]=markers[r]; r+=1
        for zkey in sorted(runs[rkey].keys()):
            added = False
            if zkey not in cdict:
                cdict[zkey]=colors[c]; c+=1
            for bkey in runs[rkey][zkey].keys():
                for qkey in runs[rkey][zkey][bkey].keys():
                    if not added:
                        lab = "$L_z=$"+zkey[1]
                        if zkey == "z4":
                            lab += ", $L_x=1$"
                        else:
                            lab += ", $L_x=$"+zkey[-1]
                        lab += ", "+rkey[1:]+r"$H^{-1}$"
                        added = True
                    else:
                        lab='_nolegend_'
                    qval = int(qkey[1:])/10.
                    if qval > 10:
                        qval = qval/10.
                    tc = runs[rkey][zkey][bkey][qkey].get_tcdata()
                    tstart = int(100*2*np.pi)
                    mean_tc = np.mean(tc[tstart:], axis=0)
                    mean_tc[2] = -mean_tc[2]
                    std_tc = np.std(tc[tstart:], axis=0)
                    if err:
                        ax.errorbar(qval, mean_tc[ind], std_tc[ind],color=cdict.get(zkey),marker=mdict.get(rkey),label=lab, capsize=2)
                    else:
                        plt.plot(qval, mean_tc[ind],color=cdict.get(zkey),marker=mdict.get(rkey),label=lab)
    plt.axhline(0.0, c="black", ls="--")
    plt.ylabel(label_dict_const[ind])
    plt.xlabel(r"Shearing parameter $q$")
    plt.legend(loc="best")
    plt.tight_layout()
    return h


#----------------------------------------------------------------
def fig_vataqe_fastest_mode(runs, f, err=False, islog=False, markers=None, colors=None):
    key_list = {"alpha_yy": 0, "alpha_xy": 1, "eta_yx" : 2, "eta_xx" : 3}
    label_dict_const = {0:r"$\alpha_{yy}=\alpha_{xx}$", 1:r"$\alpha_{xy}$", 2:"$-\eta_{yx}$", 3:"$\eta_{xx}=\eta_{yy}=\eta_t$"}
    cdict=dict(); c=0
    mdict=dict(); r=0
    if markers is None:
        markers = ["o","s","v"] #usually only have 3 resolutions
        #default colors values
    if colors is None:
        colors = ["green","blue","orange"] #usually have 2 box sizes

    h = plt.figure(f)
    ax = plt.gca()
    for rkey in sorted(runs.keys()):
        if rkey not in mdict:
            mdict[rkey]=markers[r]; r+=1
        for zkey in sorted(runs[rkey].keys()):
            added = False
            if zkey not in cdict:
                cdict[zkey]=colors[c]; c+=1
            for bkey in runs[rkey][zkey].keys():
                for qkey in runs[rkey][zkey][bkey].keys():
                    if not added:
                        lab = rkey+zkey
                        added = True
                    else:
                        lab='_nolegend_'
                    qval = int(qkey[1:])/10.
                    if qval > 10:
                        qval = qval/10.
                    tc = runs[rkey][zkey][bkey][qkey].get_tcdata()
                    tstart = int(100*2*np.pi)
                    mean_tc = np.mean(tc[tstart:], axis=0)
                    mean_tc[2] = -mean_tc[2]
                    std_tc = np.std(tc[tstart:], axis=0)

                    lambda_fast = 2*np.pi/np.sqrt(np.abs(qval*mean_tc[2]))/(2*mean_tc[3])

                    plt.plot(qval, lambda_fast,color=cdict.get(zkey),marker=mdict.get(rkey),label=lab)
    plt.ylabel(r"Fastest-growing mode wavelength $[H]$")
    plt.xlabel(r"Shearing parameter $q$")
    plt.legend(loc="upper left")
    plt.tight_layout()
    return h
#----------------------------------------------------------------
def fig_xyazte_butterfly_mode_compare(run, f):
    """
    NOTE: takes a single run!
    Compare the butterfly diagram of two largest modes to the
    full data.
    """
    two_mode_by = run.get_ftydata()
    full_mean_by = run.get_meanbydata()

    # Reconstruct full by field from two-mode data
    time_steps = two_mode_by.shape[0]
    orbit_time = np.arange(0, time_steps)/(2*np.pi)
    L_z = int(run.zval)*int(run.rval)
    z_range = np.arange(0, L_z)
    by = []
    for t in range(time_steps):
        ft = two_mode_by[t, :]
        reconstructed = ft[0]*np.cos(2*np.pi/L_z*z_range+ft[1]) + ft[2]*np.cos(4*np.pi/L_z*z_range + ft[3])
        by.append(reconstructed)
    by = np.array(by)

    # Plotting
    vmin_ = np.max([np.min(by), np.min(full_mean_by)])
    vmax_ = np.min([np.max(by), np.max(full_mean_by)])
    norm_ = colors.Normalize(vmin=vmin_, vmax=vmax_)

    extent_ = [0, time_steps/(2*np.pi), -int(run.zval)/2, int(run.zval)/2]
    aspect_ = "auto"
    fig, axes = plt.subplots(2, 1, num=f)
    ax1 = axes[0]; ax2 = axes[1]
    cax1 = ax1.imshow(np.transpose(by), extent=extent_, aspect=aspect_, norm=norm_)
    ax1.set_title("Two Mode Azimuthal B")
    ax1.set_ylabel("z [H]")
    cax2 = ax2.imshow(np.transpose(full_mean_by), extent=extent_, aspect=aspect_, norm=norm_)
    ax2.set_title("Full Azimuthal B")
    ax2.set_xlabel("Time (Orbits)")
    ax2.set_ylabel("z [H]")
    plt.tight_layout()
    cb = fig.colorbar(cax1, ax=axes.ravel().tolist())
    cb.set_label(run.idstr+"_r"+run.rval+"z"+run.zval+"b"+run.bval+"_q"+run.qval)



#----------------------------------------------------------------
def fig_xyazte_butterfly_log(run, f):
    """
    NOTE: takes a single run!
    Output a butterfly diagram with log scale.
    """
    full_mean_by = run.get_meanbydata()
    time_steps = full_mean_by.shape[0]
    orbit_time = np.arange(0, time_steps)/(2*np.pi)
    L_z = int(run.zval)*int(run.rval)
    z_range = np.arange(0, L_z)

    extent_ = [0, time_steps/(2*np.pi), -int(run.zval)/2, int(run.zval)/2]
    aspect_ = "auto"
    b_log = np.log10(np.abs(full_mean_by/np.max(np.abs(full_mean_by))))
    b_max = 0. #np.max(b_log)
    b_min = np.median(b_log)*1.4

    plt.figure(f);
    plt.imshow(np.transpose(b_log), extent=extent_, aspect=aspect_, clim=(b_min, b_max))
    ax1 = plt.gca()
    ax1.set_title(run.idstr+"_r"+run.rval+"z"+run.zval+"b"+run.bval+"_q"+run.qval)
    ax1.set_ylabel("z [H]")
    ax1.set_xlabel("Time (Orbits)")
    cb = plt.colorbar()
    cb.set_label(r"$\log_{10}(|B_y(z,t)|)$")
    plt.tight_layout()

#----------------------------------------------------------------
def fig_xyate_toymodel(run, f):
    """NOTE: takes a single run!
    Fits to the toy model presented in Lesur&Ogilvie 2008b
    and tested by SSH16 in Sec. 4.2:
    Given the filtered largest vertical modes Bx(t) and By(t),
    find gamma (cycle frequency) and beta.

    dt(Bx(t)) = gamma By(t-tr) \frac{\abs(By(t-tr))-Br}{Br}
    dt(By(t)) = -qOmega Bx(t) - beta By(t-tr)

    Here, Br and tr are constants that we guess/motivate
    from linear theory.

    XXX DOES NOT WORK XXXX

    """

    def toy_model_analytic(t, gamma, beta):
        #odeint
        return t


    def func(t, amp, omega, phi, b):
        return amp*np.sin(omega*t+phi)+b

    h = plt.figure(f)

    # Get PSD data
    psddata = run.get_psddata()
    time = psddata[:, 0]/(2*np.pi) #in orbits
    by_psd = psddata[:, 3]
    by_psd = by_psd / np.max(np.abs(by_psd)) #normalize to max value

    # Fit to sine
    paramsAll, pcovAll = curve_fit(func, time, by_psd, bounds = ([0, 0, 0, -1],[1., 2*np.pi/5., 2*np.pi, 1.]))
    r1 = get_r2(func, time, by_psd, paramsAll)
    lab = "Fit: %5.3f*sin(%5.3f t + %5.4f)+%5.4f, r2=%5.3f" % tuple(np.append(paramsAll,r1))
    plt.plot(time, by_psd, label = "r"+run.rval+"z"+run.zval+"q"+run.qval)
    plt.plot(time, func(time,paramsAll[0],paramsAll[1], paramsAll[2], paramsAll[3]),label=lab,color="black",linestyle="--")


    plt.xlabel("Time (Orbits)")
    plt.ylabel("Normalized Mean Azimuthal Field PSD")

    plt.legend()


    return h


def find_nearest_idx(array,value):
    return (np.abs(array-value)).argmin()


#----------------------------------------------------------------
def fig_xyate_psd_period(run, f, T_cycles = None):
    """NOTE: takes a single run!
    Fits the period of the cycle based off of the
    power spectrum density of the largest vertical mode.
    Need to have run convert_vtk_to_fft.py separate script
    in order to generate the necessary data files.
    See SSH16 4.1
    T_cycles = array with z, q, Tcycle
    """
    def func(t, amp, Tcycle, phi):
        return amp*np.sin(2*np.pi/Tcycle*t+phi)


    h = plt.figure(f)

    # Get PSD data
    psddata = run.get_psddata()
    time = psddata[:, 0]/(2*np.pi) #in orbits
    tstart = run.tstart
    ti = find_nearest_idx(time, tstart)
    by_psd = psddata[:, 3]
    by_psd = by_psd / np.max(np.abs(by_psd)) #normalize to max value
    # Fit only cyclic part...so after tstart
    time_fit = time[ti:]
    by_psd_fit = by_psd[ti:]
    maxT = .5*(time_fit[-1] - time_fit[0])
    print(maxT)
    # Fit to sine
    paramsAll, pcovAll = curve_fit(func, time_fit, by_psd_fit, bounds = ([0, 0, 0],[1., maxT, 2*np.pi]))
    r1 = get_r2(func, time_fit, by_psd_fit, paramsAll)
    lab = "Fit: %5.3f*sin(2*pi/%5.3f t + %5.4f), r2=%5.3f" % tuple(np.append(paramsAll,r1))
    # Plot data and the fit
    plt.plot(time, by_psd, label = "r"+run.rval+"z"+run.zval+"q"+run.qval)
    plt.plot(time, func(time,paramsAll[0],paramsAll[1], paramsAll[2]),label=lab,color="black",linestyle="--")

    plt.xlabel("Time (Orbits)")
    plt.ylabel("Normalized Mean Azimuthal Field PSD")
    plt.legend()

    if T_cycles is not None:
        if T_cycles.size == 0:
            T_cycles = np.array([float(run.zval), float(run.qval), paramsAll[1], r1])
        else:
            T_cycles = np.vstack((T_cycles, [float(run.zval), float(run.qval), paramsAll[1], r1]))
        return h, T_cycles
    else:
        return h


#-------------------------------------------------------------------
def fig_vate_fft_hst(runs,kval,f, islog=False):
    '''Plots the time evolution of fourier transform of
       data from the hst file.
    '''
    wordlabel={"Rx":[13,"Reynolds Stress"],"MEx":[10,"x Magnetic Energy"],"MEy":[11,"y Magnetic Energy"],"MEz":[12,"z Magnetic Energy"],"Mx":[19,"Maxwell Stress"]} #map kval to column number in hst file
    if kval != "Tx" and kval != "ME" and kval != "KE":
        ind = wordlabel.get(kval)[0]
    else:
        ind = None
    if ind is not None or kval == "Tx" or kval == "ME" or kval == "KE":
        h = plt.figure(f)
        for rkey in runs.keys():
            for zkey in runs[rkey].keys():
                for bkey in runs[rkey][zkey].keys():
                    for qkey in runs[rkey][zkey][bkey].keys():
                        runs[rkey][zkey][bkey][qkey].get_hdata()
                        if ind is not None:
                            data = runs[rkey][zkey][bkey][qkey].hdata[:,ind]
                        elif kval == "Tx":
                            data = ( runs[rkey][zkey][bkey][qkey].hdata[:,13] +
                                     runs[rkey][zkey][bkey][qkey].hdata[:,19] )
                            ystr = "Total Stress"
                        else:
                            if kval == "ME":
                                startind = 10
                                ystr = "Total Magnetic Energy"
                            else:
                                startind = 7 # XXX check this...kinetic energy??
                                ystr = "Total Kinetic Energy"
                            data = ( runs[rkey][zkey][bkey][qkey].hdata[:,startind] +
                                   runs[rkey][zkey][bkey][qkey].hdata[:,startind+1] +
                                   runs[rkey][zkey][bkey][qkey].hdata[:,startind+2] )

                        rval = int(rkey[1:])*int(zkey[1:])
                        print(rval)
                        print(data.shape)
                        fft_amps = np.fft.rfft(data)
                        fft_bins = np.fft.rfftfreq(data.size, 1./(rval))
                        fft_energy = np.abs(fft_amps)/(2*data.size)
                        plt.plot(fft_bins, fft_energy,label=rkey+zkey+qkey)
        plt.legend()
        plt.xlabel("Frequency (arb units)") # XX will need to change units
        if ind is None:
            plt.ylabel(ystr)
        else:
            plt.ylabel(wordlabel.get(kval)[1])
        plt.xscale('log')
        print(fft_bins)
        plt.yscale('log')
        return h
    else:
        print("Please enter a valid key ("+wordlabel.keys()+")")
        return 0

#----------------------------------------------------------------
def fig_vata_memx_nb7(runs, f, islog=False, markers=None, colors=None):
    cdict=dict(); c=0
    mdict=dict(); r=0
    if markers is None:
        markers = ["o","s","v"] #usually only have 3 resolutions
        #default colors values
    if colors is None:
        colors = ["green","blue","orange"] #usually have 2 box sizes

    h = plt.figure(f)
    ax = plt.gca()
    plt.xlabel("Shearing parameter $q$")
    plt.ylabel("Stress to Energy Ratio")
    openadded = False; lines=[]
    for rkey in runs.keys():
        if rkey not in mdict:
            mdict[rkey]=markers[r]; r+=1
        for zkey in runs[rkey].keys():
            added = False
            if zkey not in cdict:
                cdict[zkey]=colors[c]; c+=1
            for bkey in runs[rkey][zkey].keys():
                for qkey in runs[rkey][zkey][bkey].keys():
                    if not added:
                        lab = rkey+zkey
                        added = True
                    else:
                        lab='_nolegend_'
                    qval = int(qkey[1:])/10
                    if qval > 10:
                        qval = qval/10.
                    ta = runs[rkey][zkey][bkey][qkey].get_tahst()
                    line1 = ax.errorbar(qval,ta.get("Mxme")[0],ta.get("Mxme")[1],color=cdict.get(zkey),marker=mdict.get(rkey),label=lab)
                    line2 = ax.errorbar(qval,ta.get("Rxke")[0],ta.get("Rxke")[1],color=cdict.get(zkey),marker=mdict.get(rkey),markerfacecolor="none")
                    if not openadded:
                        lines.append(line1[0])
                        lines.append(line2[0])
                        openadded = True

    plt.ylim([0.,1.])
    leg2 = plt.legend(lines, ["magnetic","kinetic"],loc='best')
    plt.legend(bbox_to_anchor=(0.,1.02,1.,.102),ncol=4,mode="expand")
    #from matplotlib.legend import legend
    ax.add_artist(leg2)
    return h



#-----------------------------------------------------------------
def fig_vata_mxrx_ssh7(runs,f, islog=False, dofit=False, markers=None,colors=None):
    cdict=dict(); c=0
    mdict=dict(); r=0
    if markers is None:
        markers = ["o","s","v"] #usually only have 3 resolutions
    #default colors values
    if colors is None:
        colors = ["green","blue","orange"] #usually have 2 box sizes
    tvals=[]; t4vals=[]

    h = plt.figure(f)
    plt.xlabel(r"Shearing parameter $q$")
    plt.ylabel("Maxwell to Reynolds Stress ratio")
    for rkey in runs.keys():
        if rkey not in mdict:
            mdict[rkey]=markers[r]; r+=1
        for zkey in runs[rkey].keys():
            added = False
            if zkey not in cdict:
                cdict[zkey]=colors[c]; c+=1
            for bkey in runs[rkey][zkey].keys():
                for qkey in runs[rkey][zkey][bkey].keys():
                    if not added:
                        lab = rkey+zkey
                        added = True
                    else:
                        lab='_nolegend_'
                    qval = int(qkey[1:])/10
                    ta = runs[rkey][zkey][bkey][qkey].get_tahst()
                    trat = ta.get("Mx")[0]/ta.get("Rx")[0]
                    tstd = trat*np.sqrt((ta.get("Mx")[1]/ta.get("Mx")[0])**2+(ta.get("Rx")[1]/ta.get("Rx")[0])**2)
                    tvals.append([qval,trat])
                    if zkey == "z4":
                        t4vals.append([qval, trat])
                    plt.errorbar(qval,trat,tstd,color=cdict.get(zkey),marker=mdict.get(rkey),label=lab)


    def func(x,a,b):
        return a*(4-x)/x+b


    if dofit:
        tvals = np.array(tvals); 
        tvals = tvals[tvals[:,0].argsort()]; #sort by first column q
        if len(t4vals) > 0:
            t4vals = np.array(t4vals); t4vals = t4vals[t4vals[:,0].argsort()] 
            params4, pcov4 = curve_fit(func,t4vals[:,0],t4vals[:,1])
            r4 = get_r2(func,t4vals[:,0],t4vals[:,1],params4)
            lab4 = "z4 Fit: %5.3f*(4-q)/q+%5.3f, r2=%5.3f" % tuple(np.append(params4,r4))
            plt.plot(t4vals[:,0],func(t4vals[:,0],params4[0],params4[1]),label=lab4,color="black",linestyle=":")

        paramsAll, pcovAll = curve_fit(func,tvals[:,0],tvals[:,1])
        r1 = get_r2(func,tvals[:,0],tvals[:,1],paramsAll)
        lab1 = "All Fit: %5.3f*(4-q)/q+%5.3f, r2=%5.3f" % tuple(np.append(paramsAll,r1))
        plt.plot(tvals[:,0],func(tvals[:,0],paramsAll[0],paramsAll[1]),label=lab1,color="black",linestyle="--")
    if islog:
        plt.yscale('log')

    plt.legend()
    return h


#-----------------------------------------------------------------
def fig_vata_tx_gp1(runs,f,islog=False, dofit = False, markers=None,colors=None):
    cdict=dict(); c=0
    mdict=dict(); r=0
    if markers is None:
        markers = ["o","s","v"] #usually only have 3 resolutions
    #default colors values
    if colors is None:
        colors = ["green","blue","orange"] #usually have 2 box sizes
    tvals=[]; t4vals = []
    h = plt.figure(f)
    plt.xlabel(r"Shearing parameter $q$")
    plt.ylabel("Total Stress")
    for rkey in sorted(runs.keys()):
        if rkey not in mdict:
            mdict[rkey]=markers[r]; r+=1
        for zkey in sorted(runs[rkey].keys()):
            added = False
            if zkey not in cdict:
                cdict[zkey]=colors[c]; c+=1
            for bkey in runs[rkey][zkey].keys():
                for qkey in runs[rkey][zkey][bkey].keys():
                    if not added:
                        lab = rkey+zkey
                        added = True
                    else:
                        lab='_nolegend_'
                    qval = int(qkey[1:])/10
                    if qval > 10:
                        qval = qval/10.
                    ta = runs[rkey][zkey][bkey][qkey].get_tahst()
                    tx = ta["Tx"][0]
                    tserr = ta["Tx"][1]
                    tvals.append([qval,tx,tserr])
                    if zkey == "z4":
                        t4vals.append([qval, tx, tserr])
                    plt.errorbar(qval,tx,tserr,color=cdict.get(zkey),marker=mdict.get(rkey),label=lab)
    def func(x,a,b):
        return a * x/(2.-x)+b

    if dofit:
        tvals = np.array(tvals)
        tvals = tvals[tvals[:,0].argsort()] #sort by first column q
        if len(t4vals) > 0:
            t4vals = np.array(t4vals)
            t4vals = t4vals[t4vals[:,0].argsort()]
            params4, pcov4 = curve_fit(func,t4vals[:,0],t4vals[:,1])
            r4 = get_r2(func,t4vals[:,0],t4vals[:,1],params4)
            lab4 = "z4 Fit: %5.3f*q/(2-q)+%5.3f, r2=%5.3f" % tuple(np.append(params4,r4))
            plt.plot(t4vals[:,0],func(t4vals[:,0],params4[0],params4[1]),label=lab4,color="black",linestyle=":")
        paramsAll, pcovAll = curve_fit(func,tvals[:,0],tvals[:,1])
        r1 = get_r2(func,tvals[:,0],tvals[:,1],paramsAll)
        lab1 = "All Fit: %5.3f*q/(2-q)+%5.3f, r2=%5.3f" % tuple(np.append(paramsAll,r1))
        plt.plot(tvals[:,0],func(tvals[:,0],paramsAll[0],paramsAll[1]),label=lab1,color="black",linestyle="--")
    if islog:
        plt.yscale('log')
    plt.legend()

    return h


#-----------------------------------------------------------------
def fig_vata_tx_fitcompare(runs,f,islog=False, markers=None):
    """runs should have only one z, b, r and span a range of q."""
    mdict=dict(); r=0
    if markers is None:
        markers = ["o","s","v"] #usually only have 3 resolutions
    tvals=[]; 
    h = plt.figure(f)
    plt.xlabel(r"Shearing parameter $q$")
    plt.ylabel("Total Stress")
    for rkey in runs.keys():
        if rkey not in mdict:
            mdict[rkey]=markers[r]; r+=1
        for zkey in runs[rkey].keys():
            added = False
            for bkey in runs[rkey][zkey].keys():
                for qkey in runs[rkey][zkey][bkey].keys():
                    if not added:
                        lab = rkey+zkey
                        added = True
                        tstr = rkey+zkey+bkey
                    else:
                        lab='_nolegend_'
                    qval = int(qkey[1:])/10
                    ta = runs[rkey][zkey][bkey][qkey].get_tahst()
                    tx = ta["Tx"][0]
                    tserr = ta["Tx"][1]
                    tvals.append([qval,tx,tserr])
                    plt.errorbar(qval,tx,tserr,marker=mdict.get(rkey),color='black',label=lab)


    def ss(x,a, b):
        return a * x + b

    def abl(x,a,b):
        return a * x/(2.-x)+b

    def plaw(x,a,b):
        return a * x**b

    tvals = np.array(tvals)
    tvals = tvals[tvals[:,0].argsort()] #sort by first column q

    pSS, covSS = curve_fit(ss,tvals[:,0],tvals[:,1])
    pABL, covABL = curve_fit(abl,tvals[:,0],tvals[:,1])
    pPCP, covPCP = curve_fit(plaw, tvals[:,0], tvals[:,1], bounds=([0., 3.],[np.inf, 4.]))
    pOg, covOg = curve_fit(plaw, tvals[:,0], tvals[:,1], bounds=([0., 2.],[np.inf, 3.]))
    pKY, covKY = curve_fit(plaw, tvals[:,0], tvals[:,1], bounds=([0., 8.],[np.inf, 8.0001]))

    rSS = get_r2(ss, tvals[:,0], tvals[:,1], pSS)
    rABL = get_r2(abl,tvals[:,0],tvals[:,1],pABL)
    rPCP = get_r2(plaw,tvals[:,0],tvals[:,1],pPCP)
    rOg = get_r2(plaw,tvals[:,0],tvals[:,1],pOg)
    rKY = get_r2(plaw,tvals[:,0],tvals[:,1],pKY)

    labels = ["PCP Fit: %5.3f*q^%5.3f, r2=%5.3f" % tuple(np.append(pPCP,rPCP)),
              "Og Fit: %5.3f*q^%5.3f, r2=%5.3f" % tuple(np.append(pOg,rOg)),
              "KY Fit: %5.4f*q^%5.3f, r2=%5.3f" % tuple(np.append(pKY,rKY)),
              "SS Fit: %5.3f*q+%5.3f, r2=%5.3f" % tuple(np.append(pSS,rSS)),
              "ABL Fit: %5.3f*q/(2-q)+%5.3f, r2=%5.3f" % tuple(np.append(pABL,rABL)),
              "Data"]

    plt.plot(tvals[:,0],plaw(tvals[:,0],pPCP[0],pPCP[1]),linestyle='-')
    plt.plot(tvals[:,0],plaw(tvals[:,0],pOg[0],pOg[1]),linestyle='--',dashes=(5,2))
    plt.plot(tvals[:,0],plaw(tvals[:,0],pKY[0],pKY[1]),linestyle='--')
    plt.plot(tvals[:,0],ss(tvals[:,0],pSS[0],pSS[1]),linestyle=':')
    plt.plot(tvals[:,0],abl(tvals[:,0],pABL[0],pABL[1]),linestyle='-.')

    if islog:
        plt.yscale('log')
    plt.legend(labels)
    plt.title(tstr)
    return h

#-----------------------------------------------------------------
def fig_vata_hst(runs,kval,f,markers=None,colors=None):
    '''Plot time-averaged quantities from the .hst file. Eventually it
       would be nice to combine this function with fig_vata_mtT_plot to
       be able to plot any quantity.'''
    wordlabel={'Rx':'Reynolds Stress','MEx':'x Magnetic Energy','MEy':'y magnetic Energy','MEz':'z Magnetic Energy','Bx':'x Magnetic Field','By':'y Magnetic Field','Bz':'z Magnetic Field','Mx':'Maxwell stress','Tx':'Total Stress'}
    cdict=dict(); c=0
    mdict=dict(); r=0
    if markers is None:
        markers = ["o","s","v"] #usually only have 3 resolutions
    #default colors values
    if colors is None:
        colors = ["green","blue","orange"] #usually have 2 box sizes

    h = plt.figure(f)
    plt.xlabel(r"Shearing parameter $q$")
    plt.ylabel(wordlabel[kval])
    for rkey in sorted(runs.keys()):
        if rkey not in mdict:
            mdict[rkey]=markers[r]; r+=1
        for zkey in sorted(runs[rkey].keys()):
            added = False
            if zkey not in cdict:
                cdict[zkey]=colors[c]; c+=1
            for bkey in runs[rkey][zkey].keys():
                for qkey in runs[rkey][zkey][bkey].keys():
                    if not added:
                        lab = "$L_z=$"+zkey[1]
                        if zkey == "z4":
                            lab += ", $L_x=1$"
                        else:
                            lab += ", $L_x=$"+zkey[-1]
                        lab += ", "+rkey[1:]+r"$H^{-1}$"
                        added = True
                    else:
                        lab='_nolegend_'
                    qval = int(qkey[1:])/10
                    if qval > 10:
                        qval = qval/10.
                    ta = runs[rkey][zkey][bkey][qkey].get_tahst()
                    if kval == 'Tx':
                        plt.plot(qval,ta.get('Mx')[0]+ta.get('Rx')[0],color=cdict.get(zkey),marker=mdict.get(rkey),label=lab)
                    else:
                        plt.plot(qval,ta.get(kval)[0],color=cdict.get(zkey),marker=mdict.get(rkey),label=lab)
    if plt.gca().get_ylim()[0] <= 0:
        plt.axhline(0.0, ls="--",c="black")
    plt.legend()
    plt.tight_layout()
    return h


#-------------------------------------------------------------------
def fig_vataqe_mean_field_fit(runs, f):
    '''Plots/fits the mean field Evolution with the goal of
    extracting the dynamo growth rate. '''

    def exp_fun(t, amp, tc, gamma):
        return amp*np.exp((t-tc)*gamma)


    def lin_fit(t, slope, off):
        return slope*t+off


    def quad_fit(t, a, b, off):
        return a*t**2+off


    def get_r2(func, xdata, ydata, params):
        resids = ydata - func(xdata, *params)
        ss_res = np.sum(resids**2)
        ss_tot = np.sum((ydata - np.mean(ydata))**2)
        r_squared = 1. - (ss_res/ss_tot)
        return r_squared


    mdict=dict(); z=0
    markers = ["o","s","v"] #usually only have 3 resolutions

    win = int(10*0.1) #10 = average over one orbit
    qvals = np.array([])
    growth_rates = np.array([])
    h = plt.figure(f)
    for rkey in sorted(runs.keys()):
        for zkey in sorted(runs[rkey].keys()):
            if zkey not in mdict:
                mdict[zkey]=markers[z]; z+=1
            added = False
            for bkey in runs[rkey][zkey].keys():
                for qkey in sorted(runs[rkey][zkey][bkey].keys()):
                    if not added:
                        lab = r"$L_z=$"+zkey[1]
                        if zkey == "z4":
                            lab += ", $L_x=1$"
                        else:
                            lab += ", $L_x=$"+zkey[-1]
                        added = True
                    else:
                        lab='_nolegend_'
                    qval = int(qkey[1:])/10.
                    if qval > 10:
                        qval = qval/10.
                    if zkey == "z4":
                        qvals = np.append(qvals, qval)

                    by_data = runs[rkey][zkey][bkey][qkey].get_meanbydata()
                    bx_data = runs[rkey][zkey][bkey][qkey].get_meanbxdata()
                    bxby = np.mean(np.multiply(bx_data, by_data), axis=1)

                    bxby_smooth = moving_average(bxby, win)
                    t_orb = np.arange(bxby_smooth.shape[0])/(2*np.pi)
                    t_start = find_nearest_idx(t_orb, runs[rkey][zkey][bkey][qkey].tl1)
                    t_end = find_nearest_idx(t_orb, runs[rkey][zkey][bkey][qkey].tl2)
                    t_trimmed = t_orb[t_start:t_end]

                    bxby_trimmed = np.abs(bxby_smooth[t_start:t_end])

                    # Fit to exponential
                    p0 = [1e-3, t_start*2*np.pi, 1e-2]
                    params, pcov = curve_fit(exp_fun, t_trimmed, bxby_trimmed, p0=p0)
                    r2 = get_r2(exp_fun, t_trimmed, bxby_trimmed, params)
                    if zkey == "z4":
                        growth_rates = np.append(growth_rates, params[2])
                    plt.plot(qval, params[2], color="red", marker=mdict.get(zkey), label=lab)

    ## Fit the growth_rates to line and quadratic
    params_lin, pcov_lin = curve_fit(lin_fit, qvals, growth_rates)
    r2_lin = get_r2(lin_fit, qvals, growth_rates, params_lin)
    params_quad, pcov_quad = curve_fit(quad_fit, qvals, growth_rates)
    r2_quad = get_r2(quad_fit, qvals, growth_rates, params_quad)
#
#
    qrange = np.linspace(np.min(qvals), np.max(qvals), 500)
    plt.plot(qrange, lin_fit(qrange, *params_lin), c="black", ls=":", label=r"Linear Fit: $r^2$, {:2.2f}".format(r2_lin))
    plt.plot(qrange, quad_fit(qrange, *params_quad), c="black", ls="--", label=r"Quadratic Fit, $r^2$: {:2.2f}".format(r2_quad))
    plt.xlabel(r"Shearing parameter $q$")
    plt.ylabel("Growth rate $[\Omega]$")
    plt.legend()
    plt.tight_layout()
    #plt.show()
    return h, qvals, growth_rates
#-------------------------------------------------------------------
def fig_vate_mean_field_fit(run,f, islog=False):
    '''Plots/fits the mean field Evolution with the goal of
    extracting the dynamo growth rate.
    NOTE: takes a single run!'''


    def exp_fun(t, amp, tc, gamma):
        return amp*np.exp((t-tc)*gamma)


    def get_r2(func, xdata, ydata, params):
        resids = ydata - func(xdata, *params)
        ss_res = np.sum(resids**2)
        ss_tot = np.sum((ydata - np.mean(ydata))**2)
        r_squared = 1. - (ss_res/ss_tot)
        return r_squared

    win = int(10*0.1) #10 = average over one orbit

    by_data = run.get_meanbydata()
    bx_data = run.get_meanbxdata()
    bxby = np.mean(np.multiply(bx_data, by_data), axis=1)

    bxby_smooth = moving_average(bxby, win)
    t_orb = np.arange(bxby_smooth.shape[0])/(2*np.pi)
    t_start = find_nearest_idx(t_orb, run.tl1)
    t_end = find_nearest_idx(t_orb, run.tl2)
    t_trimmed = t_orb[t_start:t_end]

    bxby_trimmed = np.abs(bxby_smooth[t_start:t_end])

    # Fit to exponential
    p0 = [1e-3, t_start*2*np.pi, 1e-2]
    params, pcov = curve_fit(exp_fun, t_trimmed, bxby_trimmed, p0=p0)
    r2 = get_r2(exp_fun, t_trimmed, bxby_trimmed, params)

    h = plt.figure(f); f += 1
    plt.plot(t_trimmed, bxby_trimmed, label="Data")
    plt.plot(t_trimmed, exp_fun(t_trimmed, *params), label=r"Exp. fit: gamma = {:1.3f}, $r^2$: {:.3f}".format(params[2], r2))
    #plt.plot(t_orb, np.abs(bxby), label="Data")
    plt.title(run.idstr+"_r"+run.rval+"z"+run.zval+"b"+run.bval+"znf_q"+run.qval)
    if islog:
        plt.yscale('log')
    plt.legend()
    plt.xlabel("Time (orbits)")
    plt.ylabel("Mean Azimuthal Field Strength")
    plt.legend()
    plt.tight_layout()
    return h


#-------------------------------------------------------------------
def fig_vate_ME_fit(runs, f, tstart,tend, islog=False):
    '''Plots/fits the Magnetic Energy Evolution with the goal of
    extracting the dynamo growth rate. '''

    def exp_fun(t, amp, tc, gamma, off):
        return amp*np.exp((t-tc)*gamma)+off


    def get_r2(func, xdata, ydata, params):
        resids = ydata - func(xdata, *params)
        ss_res = np.sum(resids**2)
        ss_tot = np.sum((ydata - np.mean(ydata))**2)
        r_squared = 1. - (ss_res/ss_tot)
        return r_squared


    h = plt.figure(f)
    qvals = np.array([])
    growth_rates = np.array([])
    for rkey in runs.keys():
        for zkey in runs[rkey].keys():
            for bkey in runs[rkey][zkey].keys():
                for qkey in sorted(runs[rkey][zkey][bkey].keys()):
                    qvals = np.append(qvals, int(qkey[1:])/10.)

                    runs[rkey][zkey][bkey][qkey].get_hdata()
                    data = runs[rkey][zkey][bkey][qkey].hdata
                    t_orb = data[:, 0]/(2*np.pi)
                    t_start = find_nearest_idx(t_orb, tstart[rkey][zkey][bkey][qkey])
                    t_end = find_nearest_idx(t_orb, tend[rkey][zkey][bkey][qkey])
                    t_trimmed = t_orb[t_start:t_end]

                    ME_data = data[:, 10]+data[:, 11]+data[:, 12]
                    ME_trimmed = ME_data[t_start:t_end]

                    # Fit to exponential
                    params, pcov = curve_fit(exp_fun, t_trimmed, ME_trimmed)
                    r2 = get_r2(exp_fun, t_trimmed, ME_trimmed, params)
                    print(r2)
                    growth_rates = np.append(growth_rates, params[2])

                    plt.plot(t_trimmed, ME_trimmed, label=rkey+zkey+qkey+" data")
                    plt.plot(t_trimmed, exp_fun(t_trimmed, *params), label=r"Exp. fit: gamma = {:1.3f}, $r^2$: {:.3f}".format(params[2], r2))


                    if islog:
                        plt.yscale('log')
                    plt.legend()
                    plt.xlabel("Time (orbits)")
                    plt.ylabel("Total Magnetic Energy")
                    plt.legend()
                    plt.tight_layout()
    return h, qvals, growth_rates

#-------------------------------------------------------------------
def fig_vate_hst(runs,kval,f, islog=False):
    '''Plots the time evolution of data from the hst file.
       XXX Not tested or polished yet XXX'''
    wordlabel={"Rx":[13,"Reynolds Stress"],"MEx":[10,"x Magnetic Energy"],"MEy":[11,"y Magnetic Energy"],"MEz":[12,"z Magnetic Energy"],"Mx":[19,"Maxwell Stress"]} #map kval to column number in hst file
    if kval != "Tx" and kval != "ME" and kval != "KE":
        ind = wordlabel.get(kval)[0]
    else:
        ind = None
    if ind is not None or kval == "Tx" or kval == "ME" or kval == "KE":
        h = plt.figure(f)
        for rkey in runs.keys():
            for zkey in runs[rkey].keys():
                for bkey in runs[rkey][zkey].keys():
                    for qkey in sorted(runs[rkey][zkey][bkey].keys()):
                        runs[rkey][zkey][bkey][qkey].get_hdata()
                        data = runs[rkey][zkey][bkey][qkey].hdata
                        if kval == "Tx":
                            plt.plot(data[:,0]/(2*np.pi),data[:,13]+data[:,19],label=rkey+zkey+qkey)
                        elif ind is None:
                            if kval == "ME":
                                startind = 10
                            else:
                                startind = 7
                            plt.plot(data[:,0]/(2*np.pi),(data[:,startind]+data[:,startind+1]+
                                     data[:,startind+2]),label=rkey+zkey+qkey)
                        else:
                            plt.plot(data[:,0]/(2*np.pi),data[:,ind],label=rkey+zkey+qkey)
        if islog:
            plt.yscale('log')
        plt.legend()
        plt.xlabel("Time (orbits)")
        if kval == "Tx":
            plt.ylabel("Total Stress")
        elif kval == "ME":
            plt.ylabel("Total Magnetic Energy")
        elif kval == "KE":
            plt.ylabel("Total Kinetic Energy")
        else:
            plt.ylabel(wordlabel.get(kval)[1])
        return h
    else:
        print("Please enter a valid key ("+wordlabel.keys()+")")
        return 0



#--------------------------------------------------------------------
def fig_vata_mtT_plot(runs,kval,f,err=False,markers = None, colors = None):
    '''Plots the quantity indicated by the kval across shear, resolution,
    and box size. Shear is the x-axis, resolution is indicated by the
    appropriate marker, and box size is indicated via color.'''
    #default markers values
    wordlabel={'MEtot':'Total Magnetic Energy','MEmean':'Mean Field Magnetic Energy','MEymean':'Mean Azimuthal Field Magnetic Energy','MEmtr':'Ratio of mean to turbulent Magnetic Energy','MXmTr':'Ratio of mean to total Maxwell stress','MEymtr':'Ratio of mean to turbulent azimuthal field Magnetic Energy','mamean':'Mean magnetic alpha','maturb':'Turbulent magnetic alpha'}
    cdict=dict(); c=0
    mdict=dict(); r=0
    if markers is None:
        markers = ["o","s","v"] #usually only have 3 resolutions
    #default colors values
    if colors is None:
        colors = ["green","blue","orange"] #usually have 2 box sizes
    h = plt.figure(f)
    plt.xlabel(r"Shearing parameter $q$")
    plt.ylabel(wordlabel[kval])
    for rkey in sorted(runs.keys()):
        if rkey not in mdict:
            mdict[rkey]=markers[r]; r+=1
        for zkey in sorted(runs[rkey].keys()):
            added = False
            if zkey not in cdict:
                cdict[zkey]=colors[c]; c+=1
            for bkey in runs[rkey][zkey].keys():
                for qkey in runs[rkey][zkey][bkey].keys():
                    if not added:
                        lab = "$L_z=$"+zkey[1]
                        if zkey == "z4":
                            lab += ", $L_x=1$"
                        else:
                            lab += ", $L_x=$"+zkey[-1]
                        lab += ", "+rkey[1:]+r"$H^{-1}$"
                        added = True
                    else:
                        lab='_nolegend_'
                    qval = int(qkey[1:])/10.
                    if qval >= 10:
                        qval = qval/10.
                    keydata = runs[rkey][zkey][bkey][qkey].get_tadata()[kval]
                    if err:
                        plt.errorbar(qval, keydata[0], keydata[1], color=cdict.get(zkey),marker=mdict.get(rkey),label=lab, capsize=2)
                    else:
                        plt.plot(qval, keydata[0], color=cdict.get(zkey),marker=mdict.get(rkey),label=lab)
    if plt.gca().get_ylim()[0] <= 0:
        plt.axhline(0.0, ls="--",c="black")
    plt.legend(loc="best")
    plt.tight_layout()
    return h

#--------------------------------------------------------------------------------
def fig_vata_mtT_table(runs):
    '''Outputs a table that can be printed nicely. Values are important to determing
        the presence of a dynamo, as in SSH16.'''
    ta = [];
    for rkey in runs.keys():
        for zkey in runs[rkey].keys():
            for bkey in runs[rkey][zkey].keys():
                for qkey in runs[rkey][zkey][bkey].keys():
                    qval = int(qkey[1:])/10
                    rval = int(rkey[1:])
                    zval = int(zkey[1:])
                    vlist = [str(qval),str(zval),str(rval)] #add identifying columns
                    vals = list(runs[rkey][zkey][bkey][qkey].get_tadata().values())
                    for v in vals:
                        a = "{:.2E}".format(Decimal(str(v))) #consistent formatting
                        vlist.append(a)
                    if 'tn' not in locals():
                        tn = list(runs[rkey][zkey][bkey][qkey].get_tadata().keys()) #header
                        tn.insert(0,"q")
                        tn.insert(1,"z")
                        tn.insert(2,"r")
                    ta.append(vlist)#ta = np.concatenate([ta, vlist],axis=1)
    dt=('f8','i8','i8','S8','S8','S8','S8','S8','S8','S8','S8')
    t = Table(np.array(ta),names=tn,dtype=dt)
    return t


#------------------------------------------------------------------------------
def fig_xyte_by(runs,f,toscale = False,tstr = None):
    '''Outputs a butterfly diagram of each run in runs. Note that the max number
       of runs is four, otherwise they start getting too small.
    '''
    zval = []; bdata = []; nb = 0; norbs=[]; ts = False
    xlims = []; labels=[]
    if tstr is None:
        tstr=[]
        ts = True
    for rkey in runs.keys():
        for zkey in runs[rkey].keys():
            for bkey in runs[rkey][zkey].keys():
                for qkey in runs[rkey][zkey][bkey].keys():
                    zval.append(int(zkey[1:]))
                    if ts: tstr.append(rkey+zkey+bkey+qkey)
                    runs[rkey][zkey][bkey][qkey].get_bdata()
                    bdata.append(runs[rkey][zkey][bkey][qkey].bdata)
                    qval = int(qkey[1:])/10
                    nb += 1
                    if toscale:
                        norbs.append(runs[rkey][zkey][bkey][qkey].get_tend()/(2*np.pi)*qval)
                    else:
                        norbs.append(runs[rkey][zkey][bkey][qkey].get_tend()/(2*np.pi))
                    labels.append(rkey+zkey+qkey)
    nb = min(nb,4)
    h, ax1 = plt.subplots(nb,1,figsize=(6.4,6.8))
    h.canvas.manager.window.move(0,0)
    plt.suptitle(r"Azimuthal magnetic field $\langle B_y\rangle_{xy}$")

    for b in range(nb):
        ax = ax1[b]
        bfac = int(max(zval)/zval[b])
        print(bfac)
        axlim = norbs[b]
        bdat = bdata[b]
        for j in range(bfac-1):
            bdat = np.concatenate([bdat, bdata[b]],axis=1)
        im1 = ax.imshow(np.transpose(bdat),extent=[0,axlim,-max(zval)/2, max(zval)/2], aspect='auto')
        xlims.append(ax.get_xlim()[1])
        ax.axhline(-max(zval)/2+zval[b],0,1,color = 'black',ls = '--')
        ax.set_ylabel("Height (H)")
        c = h.colorbar(im1,ax=ax,ticks=np.linspace(np.min(bdata[b]),np.max(bdata[b]),4))
        c.set_label(labels[b])


    # Rescaling the x-axes
    bm = np.argmax(xlims)
    for b in range(nb):
        ax = ax1[b]
        wrat = xlims[b]/xlims[bm]
        axwidth = ax.get_position().width*wrat
        axheight = ax.get_position().height
        ax.set_position([ax.get_position().x0,ax.get_position().y0,axwidth, axheight])

    if toscale:
        plt.xlabel(r"Time ($2\pi/q$)")
    else:
        plt.xlabel("Time (orbits)")
    #plt.tight_layout() #messes with xaxis rescaling
    return h


#------------------------------------------------------------------------
def fig_compare_vate_hst_mtT(runs, f, islog = False):
    '''Compares the magnetic energy and Maxwell stresses from the
       .hst file and the vate_mtT files.
       To do: combine into one for loop.'''

    # MAGNETIC ENERGY
    h = plt.figure(f); f += 1
    hind = 10;
    tind = 1;
    for rkey in runs.keys():
        for zkey in runs[rkey].keys():
            for bkey in runs[rkey][zkey].keys():
                for qkey in runs[rkey][zkey][bkey].keys():
                    hdata = runs[rkey][zkey][bkey][qkey].get_hdata()
                    tdata = runs[rkey][zkey][bkey][qkey].get_tedata()
                    hme = hdata[:,hind]+hdata[:,hind+1]+hdata[:,hind+2]
                    plt.plot(hdata[:,0]/(2*np.pi),hme,label=rkey+zkey+qkey+" hst")
                    plt.plot(tdata[:,0]/(2*np.pi),tdata[:,tind]/2.,label=rkey+zkey+qkey+" t") # NOTE!!! directly from tedata is B2, not B2/2! Need to divide by 2 to match hst
    if islog:
        plt.yscale('log')
    plt.legend()
    plt.xlabel("Time (orbits)")
    plt.ylabel("Magnetic Energy")

    # MAXWELL STRESS
    h = plt.figure(f)
    hind = 19;
    tind = 4;
    for rkey in runs.keys():
        for zkey in runs[rkey].keys():
            for bkey in runs[rkey][zkey].keys():
                for qkey in runs[rkey][zkey][bkey].keys():
                    hdata = runs[rkey][zkey][bkey][qkey].get_hdata()
                    tdata = runs[rkey][zkey][bkey][qkey].get_tedata()
                    plt.plot(hdata[:,0]/(2*np.pi),hdata[:,hind],label=rkey+zkey+qkey+" hst")
                    plt.plot(tdata[:,0]/(2*np.pi),tdata[:,tind],label=rkey+zkey+qkey+" t")
    if islog:
        plt.yscale('log')
    plt.legend()
    plt.xlabel("Time (orbits)")
    plt.ylabel("Maxwell Stress")
    return h


#------------------------------------------------------------------------
def fig_compare_MxRx(run, f, islog = False):
    """Takes ONE run. Make sure have correct ratio of maxwell and reynolds stress,
       i.e. that Maxwell > Reynolds."""
    # FROM HST FILE
    h = plt.figure(f)
    hmind = 19; hrind = 13
    hdata = run.get_hdata()
    plt.plot(hdata[:,0]/(2*np.pi),hdata[:,hmind],label="hst Maxwell")
    plt.plot(hdata[:,0]/(2*np.pi),hdata[:,hrind],label="hst Reynolds")
    if islog:
        plt.yscale('log')
    plt.legend()
    plt.xlabel("Time (orbits)")
    plt.ylabel("Maxwell Stress")
    plt.title("r"+run.rval+"z"+run.zval+"b"+run.bval+"_q"+run.qval)
    return h



#------------------------------------------------------------------------
def figsave(idir,iname):
    '''Expanded function of savefig, to make directory if need be.
    Note: idir needs to have / at the end. '''
    if not os.path.exists(idir):
        os.makedirs(idir)
    plt.savefig(idir+iname,bbox_inches='tight')



#--------------------------------------------------------------
def get_r2(func,xdata,ydata,params):
    resids = ydata - func(xdata,*params)
    ss_res = np.sum(resids**2)
    ss_tot = np.sum((ydata - np.mean(ydata))**2)
    r_squared = 1. - (ss_res/ss_tot)
    return r_squared


# --------------------------------------------
def moving_average(a, n=3):
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n
