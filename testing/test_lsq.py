import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
from scipy.integrate import odeint


def func_Bx(X, gamma):
    x, Br = X
    return gamma*x*(x-Br)/Br


def func_By(X, beta):
    x, y, qOmega = X # x is Bx(t), y is By(t-tr)
    return -qOmega*x - beta*y


def moving_average(data, window, to_pad=False):
    # Moving average, https://stackoverflow.com/questions/13728392/moving-average-or-running-mean
    N = window # length of window
    cumsum, moving_aves = [0], []

    for i, x in enumerate(data, 1):
        cumsum.append(cumsum[i-1] + x)
        if i>=N:
            moving_ave = (cumsum[i] - cumsum[i-N])/N
            moving_aves.append(moving_ave)
    moving_aves = np.array(moving_aves)
    if to_pad:
        front = np.ones(int(np.ceil(N/2)))*data[0]
        back = np.ones(int(np.floor(N/2))-1)*data[-1]
        moving_aves = np.insert(moving_aves, 0, front)
        moving_aves = np.append(moving_aves, back)
        if moving_aves.size != data.size:
            print(data.size)
    return moving_aves


timelags = np.loadtxt("/home/hankla/analysis_sheardynamo/data_reduced/xyaze_timelag/xyaze_timelag_hgbLAIF_r64z4b4000znf_q15.txt")
fullBx1 = np.loadtxt("/home/hankla/analysis_sheardynamo/data_reduced/xyazte_ft1/xyazte_ft_bx1_hgbLAIF_r64z4b4000znf_q15.txt")
fullBy1 = np.loadtxt("/home/hankla/analysis_sheardynamo/data_reduced/xyazte_ft1/xyazte_ft_by1_hgbLAIF_r64z4b4000znf_q15.txt")

z = 128
Bx1 = fullBx1[:, z] # Bx(t)
By1 = fullBy1[:, z] # By(t)

N = 20
Bx = moving_average(Bx1, N, True)
By = moving_average(By1, N, True)
Byttr = np.roll(By, int(timelags[z])) # By(t-tr)
dtBx = np.gradient(Bx)
dtBy = np.gradient(By)

Br = .3
qOmega = 3./2
gamma_fit, gamma_cov = curve_fit(func_Bx, (Byttr, Br), dtBx)
beta_fit, beta_cov = curve_fit(func_By, (Bx, Byttr, qOmega), dtBy)

print(gamma_fit[0])
print(beta_fit[0])


# Solve ODEs given gamma, beta for Bx, By
def der_func(X, t, params):
    """
    x: Bx. y: By(t-tr)."""
    x, y = X
    Br, qOmega, gamma, beta = params

    #Br = params['Br']
    #qOmega = params['qOmega']
    #gamma = params['gamma']
    #beta = params['beta']
    xder = gamma*y*(y-Br)/Br
    yder = -qOmega*x-beta*y
    return [xder, yder]


#params = {'Br':Br, 'qOmega':qOmega, 'gamma':gamma_fit[0], 'beta':beta_fit[0]}
#params = [Br, qOmega, gamma_fit[0], beta_fit[0]]
params = [Br, qOmega, 2e-4, .08]
t = np.arange(Bx.size)
#y0 = [Bx[20], By[20]]
y0 = [0, -1]
print("y0: [{}, {}]".format(y0[0], y0[1]))
Bsol = odeint(der_func, y0, t, args=(params,))
print(Bsol[:,0])
plt.figure()
plt.plot(t, Byttr)
plt.plot(t, 10*Bx)
print(np.max(np.abs(Bsol[:,0])))
plt.figure()
#plt.plot(t, Bx/np.max(np.abs(Bx)))
Bsolsmoothx = moving_average(Bsol[:,0], N, True)
print(Bsolsmoothx)
plt.plot(t, Bsol[:,1])
plt.ylim((2, -2))
plt.show()
