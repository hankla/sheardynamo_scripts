import os
import numpy as np
'''Files need to be initiated from convert_vtk_to_py_vetab.py'''
class SingleRun:

    def __init__(self,q,r,z,b,ids,tp=None,bp=None,hp=None, ftxp=None, ftyp=None, byp=None, bxp=None, tcp=None):
        self.qval = q
        self.rval = r
        self.zval = z
        self.bval = b
        self.idstr = ids
        self.tpath = tp
        self.bpath = bp
        self.hpath = hp
        self.ftxpath = ftxp
        self.ftypath = ftyp
        self.bypath = byp
        self.bxpath = bxp
        self.tcpath = tcp
        self.tstart = 50 #in orbits (will be converted)
        self.tl1 = 10 #start of linear growth in orbits
        self.tl2 = 50 # end of lienar growth (in orbits)
        self.tedata = None
        self.tadata = None
        self.hdata = None
        self.tend = None
        self.tcdata = None


    def get_tahst(self, getstd=False):
        if self.hdata is None:
            self.hdata = self.get_hdata()
        ti = find_nearest_idx(self.hdata[:,0],self.tstart)
        self.tahst = dict()
        te = np.mean(self.hdata[ti:-1,:],axis = 0)
        tstd = np.std(self.hdata[ti:-1,:],axis = 0)

        mxme = self.hdata[ti:-1,19]/(self.hdata[ti:-1,10]+self.hdata[ti:-1,11]+self.hdata[ti:-1,12])
        self.tahst["Mxme"] = np.array([np.mean(mxme), np.std(mxme)])
        rxke = self.hdata[ti:-1,13]/(self.hdata[ti:-1,7]+self.hdata[ti:-1,8]+self.hdata[ti:-1,9])
        self.tahst["Rxke"] = np.array([np.mean(rxke), np.std(rxke)])
        tx = self.hdata[ti:-1,13]+self.hdata[ti:-1,19]
        self.tahst["Tx"] = np.array([np.mean(tx), np.std(tx)])

        self.tahst["Rx"] = np.array([te[13],tstd[13]])
        self.tahst["MEx"] = np.array([te[10], tstd[10]]) #do NOT divide by 4pi!!
        self.tahst["MEy"] = np.array([te[11], tstd[11]])
        self.tahst["MEz"] = np.array([te[12], tstd[12]])
        self.tahst["KEx"] = np.array([te[7], tstd[7]])
        self.tahst["KEy"] = np.array([te[8], tstd[8]])
        self.tahst["KEz"] = np.array([te[9], tstd[9]])
        self.tahst["Bx"] = np.array([te[16], tstd[16]])
        self.tahst["By"] = np.array([te[17], tstd[17]])
        self.tahst["Bz"] = np.array([te[18], tstd[18]])
        self.tahst["Mx"] = np.array([te[19], tstd[19]]) #do NOT divide by 4pi!!
        return self.tahst


    def get_tend(self):
        if self.tend is None:
            if self.tedata is None:
                self.tedata = self.get_tedata()
            self.tend = self.tedata[-1,0]
        return self.tend

    def get_tadata(self,getstd=False):
        if self.tedata is None:
           self.tedata = self.get_tedata() 

        self.tend = self.tedata[-1,0]
        ti = find_nearest_idx(self.tedata[:,0],self.tstart)
        self.tadata = dict()
        te = np.mean(self.tedata[ti:-1,:],axis=0)
        tstd = np.std(self.tedata[ti:-1,:],axis=0)

        memtrstd=np.std(np.array(self.tedata)[ti:-1, 3]/np.array(self.tedata)[ti:-1, 2])
        mxmtrstd=np.std(np.trim_zeros(np.array(self.tedata[ti:-1,6]/self.tedata[ti:-1,4])))
        meymtrstd=np.std(np.trim_zeros(np.array(self.tedata[ti:-1,9]/self.tedata[ti:-1,8])))
        mameanstd=np.std(np.trim_zeros(np.array(2*self.tedata[ti:-1,6]/self.tedata[ti:-1,3])))
        maturbstd=np.std(np.trim_zeros(np.array(2*self.tedata[ti:-1,5]/(self.tedata[ti:-1,1]-self.tedata[ti:-1,3]))))


        self.tadata["MEtot"] = [te[1]/2., tstd[1]/2.] #total magnetic energy. do NOT divide by 4pi!!
        self.tadata["MEmean"] = [te[3]/2., tstd[3]/2.]#mean field magnetic energy
        self.tadata["MEymean"] = [te[9]/2., tstd[9]/2.] #mean field azimuthal magnetic energy
        self.tadata["MEmtr"] = [te[3]/te[2], memtrstd]#ratio of mean to turbulent magnetic energy
        self.tadata["MXmTr"] = [te[6]/te[4], mxmtrstd] #ratio of mean to total maxwell stress
        self.tadata["MEymtr"] = [te[9]/te[8], meymtrstd] #ratio of mean to turbulent azimuthal magnetic energy
        self.tadata["mamean"] = [2*te[6]/te[3],mameanstd] #mean magnetic alpha
        self.tadata["maturb"] = [2*te[5]/(te[1]-te[3]), maturbstd] #turbulent magnetic alpha

        return self.tadata


    def get_tedata(self):
        tok = self.get_tpath()
        if tok:
            self.tedata = np.loadtxt(self.tpath)
            return self.tedata
        else:
            print('''
            ERROR! Path to time evolution data not set correctly.
            Data not loaded.
            ''')
            return 0

    def get_bdata(self):
        bok = self.get_bpath()
        if bok:
            self.bdata = np.loadtxt(self.bpath)
            return self.bdata
        else:
            print('''
            ERROR! Path to butterfly data not set correctly.
            Data not loaded.
            ''')
            return 0

    def get_hdata(self):
        hok = self.get_hpath()
        if hok:
            self.hdata = np.loadtxt(self.hpath)
            return self.hdata
        else:
            print('''
            ERROR! Path to .hst file not set correctly.
            Data not loaded.
            ''')
            return 0


    def get_ftxdata(self):
        tok = self.get_ftxpath()
        if tok:
            print(self.ftxpath)
            self.ftxdata = np.loadtxt(self.ftxpath)
            return self.ftxdata
        else:
            print('''
            ERROR! Path to FTx data not set correctly.
            Data not loaded.
            ''')
            return 0


    def get_ftydata(self):
        tok = self.get_ftypath()
        if tok:
            print(self.ftypath)
            self.ftydata = np.loadtxt(self.ftypath)
            return self.ftydata
        else:
            print('''
            ERROR! Path to FTy data not set correctly.
            Data not loaded.
            ''')
            return 0


    def get_meanbydata(self):
        tok = self.get_bypath()
        if tok:
            self.meanbydata = np.loadtxt(self.bypath)
            return self.meanbydata
        else:
            print('''
            ERROR! Path to by data not set correctly.
            Data not loaded.
            ''')
            return 0


    def get_meanbxdata(self):
        tok = self.get_bxpath()
        if tok:
            self.meanbxdata = np.loadtxt(self.bxpath)
            return self.meanbxdata
        else:
            print('''
            ERROR! Path to bx data not set correctly.
            Data not loaded.
            ''')
            return 0


    def get_tcdata(self):
        tok = self.get_tcpath()
        if tok:
            #print(self.tcpath)
            self.tcdata = np.loadtxt(self.tcpath)
            return self.tcdata
        else:
            print('''
            ERROR! Path to tc data not set correctly.
            Data not loaded.
            ''')
            return 0


    def get_tpath(self,tp=None):
        tp = tp if tp is not None else self.tpath
        if tp is None:
            tp = "/home/hankla/analysis_sheardynamo/data_reduced/vate_mtT/vate_mtT_"
            tp+=self.idstr+"_r"+self.rval+"z"+self.zval+"b"+self.bval+"znf_q"+self.qval+".txt"
        if not os.path.isfile(tp):
            print('''
            ERROR!
            Tried to change path to '''+tp+''' but file does not exist!
            Please set the time evolution data path (set tpath) and call get_vate_mtT to generate the time evolution data
            Path is currently set to '''+str(self.tpath)+"\n")
            return 0
        else:
            self.tpath = tp
            return 1

    def get_bpath(self,bp=None):
        bp = bp if bp is not None else self.bpath
        if bp is None:
            bp = "/home/hankla/analysis_sheardynamo/data_reduced/xyate_by/xyate_by_"
            bp+=self.idstr+"_r"+self.rval+"z"+self.zval+"b"+self.bval+"znf_q"+self.qval+".txt"
        if not os.path.isfile(bp):
            print('''
            ERROR!
            Tried to change path to '''+bp+''' but file does not exist!
            Please set the butterfly data path (set_bpath) and call get_xyate_by to generate the butterfly data
            Path is currently set to '''+str(self.bpath)+"\n")
            return 0
        else:
            self.bpath = bp
            return 1

    def get_hpath(self,hp=None):
        hp = hp if hp is not None else self.hpath #if not specified, hp is hpath
        if hp is None:
            hp = "/home/hankla/analysis_sheardynamo/data_reduced/vate_hst/vate_hst_"
            hp+=self.idstr+"_r"+self.rval+"z"+self.zval+"b"+self.bval+"znf_q"+self.qval+".hst"
        if not os.path.isfile(hp):
            print('''
            ERROR!
            Tried to change path to '''+hp+''' but file does not exist!
            Please set the .hst data path (set_hpath) correctly.
            Path is currently set to '''+str(self.hpath)+"\n")
            return 0
        else:
            self.hpath = hp
            return 1


    def get_mpath(self, mp = None):
        mp = mp if mp is not None else self.mpath #if not specified, mp is mpath
        if mp is None:
            # path to vtk files is complicated.
            mp = "/home/hankla"
        if not os.path.isfile(mp):
            print('''
            ERROR!
            Tried to change path to '''+mp+''' but file does not exist!
            Please set the .hst data path (set_mpath) correctly.
            Path is currently set to '''+str(self.mpath)+"\n")
            return 0
        else:
            self.vpath = mp
            return 1


    def get_ftypath(self, fty = None):
        fty = fty if fty is not None else self.ftypath #if not specified, ftx is ftxpath
        if fty is None:
            fty = "/home/hankla/analysis_sheardynamo/data_reduced/xyate_ft12/xyate_ft12_by_"
            fty += self.idstr+"_r"+self.rval+"z"+self.zval+"b"+self.bval+"znf_q"+self.qval+".txt"
        if not os.path.isfile(fty):
            print('''
            ERROR!
            Tried to change path to '''+fty+''' but file does not exist!
            Please set the ftx data path (set_ftypath) correctly.
            Path is currently set to '''+str(self.ftypath)+"\n")
            return 0
        else:
            self.ftypath = fty
            return 1


    def get_ftxpath(self, ftx = None):
        ftx = ftx if ftx is not None else self.ftxpath #if not specified, ftx is ftxpath
        if ftx is None:
            ftx = "/home/hankla/analysis_sheardynamo/data_reduced/xyate_ft12/xyate_ft12_bx_"
            ftx += self.idstr+"_r"+self.rval+"z"+self.zval+"b"+self.bval+"znf_q"+self.qval+".txt"
        if not os.path.isfile(ftx):
            print('''
            ERROR!
            Tried to change path to '''+ftx+''' but file does not exist!
            Please set the ftx data path (set_ftxpath) correctly.
            Path is currently set to '''+str(self.ftxpath)+"\n")
            return 0
        else:
            self.ftxpath = ftx
            return 1


    def get_bypath(self, by = None):
        by = by if by is not None else self.bypath #if not specified, psd is psdpath
        if by is None:
            by = "/home/hankla/analysis_sheardynamo/data_reduced/xyazte_mean_field/xyazte_by_"
            by += self.idstr+"_r"+self.rval+"z"+self.zval+"b"+self.bval+"znf_q"+self.qval+".txt"
        if not os.path.isfile(by):
            print('''
            ERROR!
            Tried to change path to '''+by+''' but file does not exist!
            Please set the by data path (set_bypath) correctly
            Path is currently set to '''+str(self.bypath)+"\n")
            return 0
        else:
            self.bypath = by
            return 1


    def get_bxpath(self, bx = None):
        bx = bx if bx is not None else self.bxpath #if not specified, psd is psdpath
        if bx is None:
            bx = "/home/hankla/analysis_sheardynamo/data_reduced/xyazte_mean_field/xyazte_bx_"
            bx += self.idstr+"_r"+self.rval+"z"+self.zval+"b"+self.bval+"znf_q"+self.qval+".txt"
        if not os.path.isfile(bx):
            print('''
            ERROR!
            Tried to change path to '''+bx+''' but file does not exist!
            Please set the bx data path (set_bxpath) correctly
            Path is currently set to '''+str(self.bxpath)+"\n")
            return 0
        else:
            self.bxpath = bx
            return 1


    def get_tcpath(self, tc = None):
        tc = tc if tc is not None else self.tcpath #if not specified, psd is psdpath
        if tc is None:
            tc = "/home/hankla/analysis_sheardynamo/data_reduced/vate_transport_coefficients/vate_transport_coefficients_const_"
            tc += self.idstr+"_r"+self.rval+"z"+self.zval+"b"+self.bval+"znf_q"+self.qval+".txt"
        if not os.path.isfile(tc):
            print('''
            ERROR!
            Tried to change path to '''+tc+''' but file does not exist!
            Please set the tc data path (set_tcpath) correctly
            Path is currently set to '''+str(self.tcpath)+"\n")
            return 0
        else:
            self.tcpath = tc
            return 1


    def set_tpath(self,tp):
        if self.get_tpath(tp):
            self.tpath = tp

    def set_bpath(self,bp):
        if self.get_bpath(bp):
            self.bpath = bp

    def set_hpath(self,hp):
        if self.get_hpath(hp):
            self.hpath = hp


    def set_mpath(self, mp):
        if self.get_mpath(mp):
            self.mpath = mp

    def set_tcpath(self, tc):
        if self.get_mpath(tc):
            self.tcpath = tc


    def set_ftxpath(self, ftx):
        if self.get_ftxpath(ftx):
            self.ftxpath = ftx


    def set_ftypath(self, fty):
        if self.get_ftypath(fty):
            self.ftypath = fty


    def set_bxpath(self, bx):
        if self.get_bypath(bx):
            self.bypath = bx


    def set_bypath(self, by):
        if self.get_bypath(by):
            self.bypath = by


def find_nearest_idx(array,value):
    return (np.abs(array-value)).argmin()


def initialize(trialstr,ids):
    runs = dict()
    for ts in trialstr:
        rs = ts[ts.find('r'):ts.find('z')]
        zs = ts[ts.find('z'):ts.find('b')]
        bs = ts[ts.find('b'):ts.find('q')]
        qs = ts[ts.find('q'):]
        if rs not in runs:
            runs[rs]=dict()
        if zs not in runs[rs]:
            runs[rs][zs]=dict()
        if bs not in runs[rs][zs]:
            runs[rs][zs][bs]=dict()
        runs[rs][zs][bs][qs] = SingleRun(qs[1:],rs[1:],zs[1:],bs[1:],ids)
    return runs
